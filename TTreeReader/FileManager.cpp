#include "FileManager.hpp"

FileManager::FileManager(std::string directory)
{
    this->directory = directory;
    std::cout << "Looking for root files in directory " << directory << std::endl;
    for (const auto & entry : std::filesystem::directory_iterator(directory)) {
        if(entry.path().extension() == ".root") {
            files.push_back(entry);
        }
    }
    if(files.size() == 0) {
        std::cout << "No root files found in directory " << directory << std::endl;
        throw std::invalid_argument("No root files found in directory");
    }
    std::sort(files.begin(), files.end());
    std::cout << "Found " << files.size() << " root files in directory " << directory << std::endl;
}

FileManager::FileManager() {}

FileManager::~FileManager()
{
    //delete reader;
    //delete sCont;
}

void FileManager::listFiles()
{
    for (const auto & entry : files) {
        std::cout << entry.path() << std::endl;
    }
}

void FileManager::parseReader(TTreeReader* reader_)
{
    reader = reader_;
    //for(auto& file: files) {
    //    try{
    //        reader->parseTree(file.path());
    //    }
    //    catch(std::exception &e) {
    //        std::cout << "No events in: " << file.path() << "\n";
    //        continue;
    //    }
    //    return;
    //}
}

void FileManager::loadEventNumber(int nEv)
{
    reader->loadEventNumber(nEv);
    //reader->printEvent();
}

bool FileManager::searchForSuperID(unsigned superID, bool fast)
{
    //automatize this so it starts from the last file used
    int fileNumber = 0;
    for(auto& file: files) {
        try{
            std::cout << "Searching in file: " << file.path() << std::endl;
            std::cout << "Searching in file: " << file.path().string() << std::endl;
            reader->parseTree(file.path().string());
        }
        catch(std::exception &e) {
            std::cout << "No events in: " << file.path() << "\n";
            continue;
        }
        int entry;
        if(!fast) {
            entry = reader->searchForSuperIDSlow(superID);
        }
        else {
            entry = reader->searchForSuperID(superID);
        }
        if(entry != -1) {
            //std::cout << "Parsed file " << file.path() << std::endl; // atm is opening and closing all the files :s
            reader->loadEventNumber(entry);
            current_filename = file.path().string();
            std::cout << "Found superID " << superID << " in file " << current_filename << " at entry: " << entry << std::endl;
            currentSuperIDindex.filename = current_filename;
            currentSuperIDindex.entry = entry;
            currentSuperIDindex.superID = reader->SuperID->at(0);
            currentSuperIDindex.filenumber = fileNumber;
            std::cout << "Current superID index:      " << currentSuperIDindex.superID << std::endl;
            std::cout << "Current superID filename:   " << currentSuperIDindex.filename << std::endl;
            std::cout << "Current superID entry:      " << currentSuperIDindex.entry << std::endl;
            std::cout << "Current superID filenumber: " << currentSuperIDindex.filenumber << std::endl;
            return true;
        }
        fileNumber++;
        reader->closeTree();
    }
    std::cout << "SuperID not found" << std::endl;
    return false;
}

bool FileManager::searchForFirstSuperIDSameOrGreater(const unsigned superID)
{
    int fileIndex = 0;
    for (auto &file : files)
    {
        try{
            reader->parseTree(file.path());
        }
        catch(std::exception &e) {
            std::cout << "No events in: " << file.path() << "\n";
            continue;
        }
        int entry = reader->searchForFirstSuperIDSameOrGreater(superID);
        // std::cout << "Parsed file " << file.path() << std::endl;
        if (entry != -1)
        {
            reader->loadEventNumber(entry);
            current_filename = file.path();

            std::cout << "Found superID " << reader->SuperID->at(0) << " first super >= " << superID << " in file " << current_filename << " at entry: " << entry << std::endl;
            currentSuperIDindex.filename = current_filename;
            currentSuperIDindex.entry = entry;
            currentSuperIDindex.superID = reader->SuperID->at(0);
            currentSuperIDindex.filenumber = fileIndex;
            return true;
        }
        fileIndex++;
        reader->closeTree();
    }
    std::cout << "SuperID not found" << std::endl;
    return false;
}

void FileManager::searchForSuperIDRange(unsigned superIDStart, unsigned superIDEnd) {
    if(!searchForFirstSuperIDSameOrGreater(superIDStart)) {
        std::cout << "SuperID " << superIDStart << " not found" << std::endl;
        return;
    }
    firstSuperIDindex = currentSuperIDindex;
    std::cout << "Found first superID " << superIDStart << " in file " << firstSuperIDindex.filename << " at entry: " << firstSuperIDindex.entry << std::endl;
    if (!searchForFirstSuperIDSameOrGreater(superIDEnd)) {
        std::cout << "SuperID " << superIDEnd << " not found" << std::endl;
        return;
    }
    lastSuperIDindex = currentSuperIDindex;
    std::cout << "Found last superID " << superIDEnd << " in file " << lastSuperIDindex.filename << " at entry: " << lastSuperIDindex.entry << std::endl;
}

void FileManager::bindStubContainer(StubContainer *sCont_) {
    std::cout << "Binding stub container" << std::endl;
    sCont = sCont_;
    std::cout << "sCont binded" << std::endl;
}

void FileManager::loadAllStubsInContainer() {
    std::cout << "Loading all stubs in container" << std::endl;
    //load file with first superID
    //throw error if firstSuperIDindex is not set
    if(firstSuperIDindex.filename == "") {
        std::cout << "First superID not set" << std::endl;
        throw std::invalid_argument("First superID not set");
    }
    //or second superID
    if(lastSuperIDindex.filename == "") {
        std::cout << "Last superID not set" << std::endl;
        throw std::invalid_argument("Last superID not set");
    }
    //set currentFileNumber to firstSuperIDindex file

    reader->parseTree(firstSuperIDindex.filename);
    currentFileNumber = firstSuperIDindex.filenumber;
    reader->loadEventNumber(firstSuperIDindex.entry);
    std::cout << "Loaded first superID " << reader->SuperID->at(0) << " in file " << firstSuperIDindex.filename << " at entry: " << firstSuperIDindex.entry << std::endl;
    //load all stubs until last superID
    while (reader->SuperID->at(0) <= lastSuperIDindex.superID)
    {
        auto stubs = reader->getStubsFromLoadedEvent();
        for (auto &stub : stubs) {
            sCont->addStub(stub);
        }
        //if loadEventNumber > nEntries, load next file on the parser
        if(reader->currentEvNumber + 1 >= reader->eventTree->GetEntries()) {
            std::cout << "Exceeded length - going to the next file" << std::endl;
            std::cout << "Loading tree: " << files[currentFileNumber + 1].path() << " actual superID: " << reader->SuperID->at(0) << std::endl;
            reader->closeTree();
            reader->parseTree(files[currentFileNumber + 1].path());
            currentFileNumber++;
            std::cout << "Ev number " << reader->currentEvNumber << std::endl;
        }
        reader->loadNextEvent();
        std::cout << "Ev number: " << reader->currentEvNumber << std::endl;
    }
}

void FileManager::loadNextSuperIDInContainer(int nSuperID)
{
    std::cout << "Loading all stubs in container" << std::endl;
    // load file with first superID
    // throw error if firstSuperIDindex is not set
    if (currentSuperIDindex.filename == "") {
        throw std::invalid_argument("Current superID not set");
    }
    std::cout << "in load next superID" << std::endl;
    std::cout << "Current superID index:      " << currentSuperIDindex.superID << std::endl;
    std::cout << "Current superID filename:   " << currentSuperIDindex.filename << std::endl;
    std::cout << "Current superID entry:      " << currentSuperIDindex.entry << std::endl;
    std::cout << "Current superID filenumber: " << currentSuperIDindex.entry << std::endl;

    reader->parseTree(currentSuperIDindex.filename);
    currentFileNumber = currentSuperIDindex.filenumber;
    reader->loadEventNumber(currentSuperIDindex.entry);
    //std::cout << "Loaded first superID " << reader->SuperID->at(0) << " in file " << firstSuperIDindex.filename << " at entry: " << firstSuperIDindex.entry << std::endl;
    // load all stubs until last superID
    //getchar();
    unsigned superIDtoStopAt = currentSuperIDindex.superID + nSuperID;
    while (reader->SuperID->at(0) < superIDtoStopAt)
    {
        auto stubs = reader->getStubsFromLoadedEvent();
        std::cout << "Getting stubs from loaded event" << std::endl;
        std::cout << "Got: " << stubs.size() << " stubs" << std::endl;
        for (auto &stub : stubs)
        {
            sCont->addStub(stub); //Up to here I'm getting stubs
        }
        // if loadEventNumber > nEntries, load next file on the parser
        if (reader->currentEvNumber + 1 >= reader->eventTree->GetEntries()) {
            //return;
            std::cout << "Exceeded length - going to the next file - loading superID" << std::endl;
            std::cout << "Loading tree: " << files[currentFileNumber + 1].path() << " actual superID: " << reader->SuperID->at(0) << std::endl;
            reader->parseTree(files[currentFileNumber + 1].path());
            std::cout << "Ev number " << reader->currentEvNumber << std::endl;
            //getchar();
            currentFileNumber++;
        }
        reader->loadNextEvent();
        //std::cout << "Ev number " << reader->currentEvNumber << std::endl;

        currentSuperIDindex.filename = files[currentFileNumber].path();
        currentSuperIDindex.entry = reader->currentEvNumber;
        currentSuperIDindex.superID = reader->SuperID->at(0);
        currentSuperIDindex.filenumber = currentFileNumber;
    }
}

void FileManager::loadSuperIDInContainer(unsigned superID)
{
    if(searchForSuperID(superID)) {
        loadNextSuperIDInContainer(100);
    }
}

void FileManager::cleanFiles() {
    //parse each file
    //if file is empty, remove it from the list
    //if file is not empty, add it to the list
    std::cout << "Removing empty files\n";
    std::vector<std::filesystem::directory_entry> newFiles;
    for (const auto & entry : files) {
        try {
            reader->parseTree(entry.path());
        }
        catch (std::exception &e) {
            std::cout << "No events in: " << entry.path() << "\n";
            continue;
        }
        if(!reader->treeIsEmpty()) {
            newFiles.push_back(entry);
        }
        else{
            std::cout << "Removing file " << entry.path() << "\n";
        }
        //std::cout << "Closing tree" << std::endl;
        reader->closeTree();
        //std::cout << "Closed tree" << std::endl;
    }
    files = newFiles;
    std::cout << "Done\n";
}


void FileManager::setFilenames(std::vector<std::string> newFileNames) {
    filenames = newFileNames;
}

std::vector<std::string> FileManager::getFilenames() {
    return filenames;
}