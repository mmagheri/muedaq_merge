add_library(
  TTreeReader
  TTreeReader.cpp
  FileManager.cpp
)

find_package(ROOT 6.16 CONFIG REQUIRED)

target_include_directories(
    TTreeReader PUBLIC
    ../StubContainer
    ${ROOT_INCLUDE_DIRS}

)

target_link_libraries(
    TTreeReader PUBLIC
    StubContainer
    ${ROOT_LIBRARIES}
)