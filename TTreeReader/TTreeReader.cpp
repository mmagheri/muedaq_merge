#include "TTreeReader.hpp"

TTreeReader::~TTreeReader()
{
    //delete eventTree;
    //treeFile->Close();
    delete SuperID;
    delete Bx;
    delete Link;
    delete Bend;
    delete LocalX;
    delete LocalY;
}

void TTreeReader::parseTree(std::string filename_)
{
    filename = filename_;
    treeFile = TFile::Open(filename.c_str(), "READ");
    eventTree = (TTree *)treeFile->Get("cbmsim");
    eventChain.SetBranchAddress("SuperID", &SuperID);
    eventChain.SetBranchAddress("Persistency", &Persistency);
    eventChain.SetBranchAddress("Bx", &Bx);
    eventChain.SetBranchAddress("Link", &Link);
    eventChain.SetBranchAddress("Bend", &Bend);
    eventChain.SetBranchAddress("LocalX", &LocalX);
    eventChain.SetBranchAddress("LocalY", &LocalY);
    if(isVcthScan) {
        eventChain.SetBranchAddress("UserBits", &UserBits);
        eventChain.SetBranchAddress("ModuleID_vcthScan", &ModuleID);
        eventChain.SetBranchAddress("Threshold_vcthScan", &Threshold);
    }
    std::cout << "Branch set: " << filename << std::endl;
    currentEvNumber = -1;
    std::cout << "parsed tree in : " << filename << std::endl;
    std::cout << "We have: " << eventChain.GetEntries() << " events" << std::endl;
    if (eventChain.GetEntries() == 0)
    {
        throw std::invalid_argument("No events in tree " + filename);
    }
}

void TTreeReader::parseChain(std::vector<std::string> filenames) {
    //reverse order in the vector
    //std::reverse(filenames.begin(), filenames.end());
    for(auto& f: filenames) {
        //check if file exists
        if(!std::filesystem::exists(f)) {
            std::cout << "File " << f << " does not exist" << std::endl;
            throw std::invalid_argument("File " + f + " does not exist");
        }
        eventChain.Add(f.c_str());
    }
    eventChain.SetBranchAddress("SuperID", &SuperID);
    eventChain.SetBranchAddress("Bx", &Bx);
    eventChain.SetBranchAddress("Link", &Link);
    eventChain.SetBranchAddress("Bend", &Bend);
    eventChain.SetBranchAddress("LocalX", &LocalX);
    eventChain.SetBranchAddress("LocalY", &LocalY);
    eventChain.SetBranchAddress("Persistency", &Persistency);
    if(isVcthScan) {
        eventChain.SetBranchAddress("UserBits", &UserBits);
        eventChain.SetBranchAddress("ModuleID_vcthScan", &ModuleID);
        eventChain.SetBranchAddress("Threshold_vcthScan", &Threshold);
    }

    std::cout << "parsed chain" << std::endl;
}

// set station

void TTreeReader::loadEventNumber(int nEv)
{
    currentEvNumber = nEv;
    if(nEv > eventChain.GetEntries()) {
        std::cout << "Event number " << nEv << " is out of range. Max number of events is " << eventChain.GetEntries() << std::endl;
        throw std::invalid_argument("Evt number higher than max number of events");
        //throw exception
        return;
    }
    //std::cout << "Loaded event: " << nEv << std::endl;
    if(nEv == -1) eventChain.GetEntry(eventChain.GetEntries()-1);
    eventChain.GetEntry(currentEvNumber);
}

int TTreeReader::searchForSuperID(unsigned superID)
{
    int low = 0, high = eventChain.GetEntries() - 1;

    // Edge case checks
    loadEventNumber(low);
    if (SuperID->at(0) > superID) {
        return -1;
    }
    loadEventNumber(high);
    if (SuperID->at(0) < superID) {
        return -1;
    }

    while (low <= high) {
        int mid = (low + high) / 2;
        loadEventNumber(mid);

        if (SuperID->at(0) == superID) {
            return mid;
        }
        else if (SuperID->at(0) < superID) {
            low = mid + 1;
        }
        else {
            high = mid - 1;
        }
    }
    return -1;
}

int TTreeReader::searchForSuperIDSlow(unsigned superID)
{
    // load first and last entry and continue only if superID is in range
    loadEventNumber(0);
    if (SuperID->at(0) > superID) {
        return -1;
    }
    loadEventNumber(eventChain.GetEntries() - 1);
    if (SuperID->at(0) < superID) {
        return -1;
    }
    for (int entry = 0; entry < eventChain.GetEntries(); entry++) {
        loadEventNumber(entry);
        if (SuperID->at(0) == superID)
        {
            return entry;
        }
    }
    return -1;
}

int TTreeReader::searchForFirstSuperIDSameOrGreater(unsigned superID)
{
    // Assumes the superID are sorted in ascending order

    loadEventNumber(eventChain.GetEntries() - 1);
    if (SuperID->at(0) < superID)
    {
        return -1;
    }
    loadEventNumber(0);
    if (SuperID->at(0) > superID)
    {
        return 0;
    }
    for (int entry = 0; entry < eventChain.GetEntries(); entry++)
    {
        loadEventNumber(entry);
        if (SuperID->at(0) >= superID)
        {
            return entry;
        }
    }
    return -1;
}

int TTreeReader::searchForBxID(unsigned bxID, int nEtryToStart)
{
    for (int entry = nEtryToStart; entry < eventChain.GetEntries(); entry++) {
        loadEventNumber(entry);
        if (Bx->at(0) == bxID) {
            return entry;
        }
    }
    return -1;
}

void TTreeReader::closeTree()
{
    treeFile->Close();
    //delete eventTree;
}

bool TTreeReader::treeIsEmpty()
{
    return eventChain.GetEntries() == 0;
}

void TTreeReader::printEvent()
{
    std::cout << "Event number: " << currentEvNumber << std::endl;
    for (size_t i = 0; i < SuperID->size(); i++) {
        std::cout << "SuperID: " << SuperID->at(i) << " Bx: " << Bx->at(i) << " Link: " << Link->at(i) << " Bend: " << Bend->at(i) << " LocalX: " << LocalX->at(i) << " LocalY: " << LocalY->at(i) << std::endl;
    }
}

bool TTreeReader::loadNextEvent()
{
    currentEvNumber++;
    if(currentEvNumber == eventChain.GetEntries()) {
        std::cout << "LAST EVENT" << std::endl;
        Persistency->clear();
        Bx->clear();
        SuperID->clear();
        LocalX->clear();
        LocalY->clear();
        Link->clear();
        return false;
    }
    loadEventNumber(currentEvNumber);
    return true;
}

std::vector<Stub> TTreeReader::getStubsFromLoadedEvent() {
    std::vector<Stub> stubs;
    for(size_t i = 0; i < SuperID->size(); i++) {
        Stub tmpStub(SuperID->at(i), Bx->at(i), Bend->at(i), LocalX->at(i), LocalY->at(i), Link->at(i));
        tmpStub.setUserbits(UserBits->at(i));
        //tmpStub.setPersistency(Persistency->at(i)); //PERSISTENCY NOT WORKING CORRECTLY - BUT removed,to be fixed
        stubs.push_back(tmpStub);
    }
    return stubs;
}

void TTreeReader::testChain() {
    eventChain.GetEntry(0);
    printEvent();
    eventChain.GetEntry(eventChain.GetEntries()-1);
    printEvent();

}

uint32_t TTreeReader::getLoadedSuperID() {
    if(SuperID->size() != 0) {
        return SuperID->at(0);
    } else {
        std::cout << "No SuperID loaded" << std::endl;
        return std::numeric_limits<unsigned>::max();
    }
}

void TTreeReader::setVcthScan(bool scan_) {
    isVcthScan = scan_;
}