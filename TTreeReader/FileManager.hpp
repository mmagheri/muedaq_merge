#include<filesystem>
#include <iostream>
#include <fstream>
#include <string>
#include "TTreeReader.hpp"
#include "StubContainer.hpp"


struct superIDindex
{
    std::string filename = "";
    int filenumber = -1;
    int entry = -1;
    unsigned superID = 0;
};

class FileManager
{
private:
    std::string directory;
    std::string current_filename;
    std::vector<std::filesystem::directory_entry> files;
    std::vector<std::string> filenames;
    StubContainer *sCont;

    superIDindex firstSuperIDindex, lastSuperIDindex, currentSuperIDindex; // filename, entry
    
    int currentFileNumber = -999;

public:
    TTreeReader* reader;
    FileManager(std::string directory);
    FileManager();
    ~FileManager();
    void setFilenames(std::vector<std::string> newFileNames);
    std::vector<std::string> getFilenames();
    void listFiles();
    void cleanFiles();
    void parseReader(TTreeReader *reader_);
    void loadEventNumber(const int nEv);
    bool searchForSuperID(const unsigned superID, bool fast = true);
    bool searchForFirstSuperIDSameOrGreater(const unsigned superID);
    void searchForSuperIDRange(const unsigned superIDStart, const unsigned superIDEnd);
    void bindStubContainer(StubContainer *sCont_);
    void loadAllStubsInContainer();
    void loadNextSuperIDInContainer(int nSuperID = 10);
    void loadSuperIDInContainer(unsigned superID);
};