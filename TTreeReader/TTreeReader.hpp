#pragma once

#include <vector>
#include <string>
#include <stdint.h>
#include <iostream>
#include <map>
#include <stdexcept>
#include <filesystem>
#include "Stub.hpp"

///////////////
// ROOT HEADERS
#include <TROOT.h>
#include <TTree.h>
#include <TFile.h>
#include <TChain.h>

class TTreeReader
{
protected:
    const std::string TREENAME = "cbmsim";
    std::string filename;

public:
    int currentEvNumber = 0;
    TTree *eventTree;
    TChain eventChain;
    std::vector<unsigned short> *Bx = 0;
    std::vector<unsigned short> *Link = 0;
    std::vector<float> *Bend = 0;
    std::vector<float> *LocalX = 0;
    std::vector<float> *LocalY = 0;
    std::vector<unsigned int> *SuperID = 0;
    std::vector<unsigned int> *Persistency = 0;
    std::vector<unsigned int> *UserBits = 0;
    std::vector<unsigned int> *ModuleID = 0;
    std::vector<unsigned int> *Threshold = 0;

    bool isVcthScan = false;
     
    TFile *treeFile;
    ~TTreeReader();
    void parseTree(std::string filename);
    void parseChain(std::vector<std::string> filenames);
    TTreeReader() : currentEvNumber(0), eventChain("cbmsim") { };
    void loadEventNumber(int nEv);
    bool loadNextEvent();
    int searchForSuperID(unsigned superID);
    int searchForSuperIDSlow(unsigned superID);
    int searchForFirstSuperIDSameOrGreater(unsigned superID);
    int searchForBxID(unsigned bxID, int nEtryToStart = 0);
    void closeTree();
    bool treeIsEmpty();
    std::vector<Stub> getStubsFromLoadedEvent();

    void setVcthScan(bool scan_);
    uint32_t getLoadedSuperID();
    void printEvent();
    void testChain();
};
