#include <TTree.h>
#include "FileManager.hpp"
#include "Querier.hpp"
#include "StubContainer.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <filesystem>
#include <algorithm>
#include "parser.hpp"
#include <time.h>

int main(int argc, char *argv[])
{
    std::string outputFileName;
    std::string firstRunDirectory;
    std::string secondRunDirectory;
    unsigned firstSuperID = 8027516;
    unsigned range = 100000;
    std::string runNumber = "4018";
    std::string runNumber2 = "4017";
    //3238 - 3237 seems like good runs
    std::string rawPath = "/eos/experiment/mu-e/staging/daq/2023/raw/commissioning/";
    //std::string decodedPath = "/eos/experiment/mu-e/staging/daq/2023/decoded/commissioning/";
    std::string decodedPath = "/muedaq_merge/";
    Querier *querier = new Querier();
    querier->setUrl("http://muedaq-supervisor/api/v1/data/raw/" + runNumber + "/query");
    querier->setFolder(rawPath + "run_" + runNumber + "/");
    auto goodFiles = querier->getFilesInRange(firstSuperID, range);
    querier->setUrl("http://muedaq-supervisor/api/v1/data/raw/" + runNumber2 + "/query");
    querier->setFolder(rawPath + "run_" + runNumber2 + "/");
    auto goodFiles2 = querier->getFilesInRange(firstSuperID, range);
    std::cout  << "Good files: " << std::endl;
    //get just the filename from the path
    std::transform(goodFiles.begin(), goodFiles.end(), goodFiles.begin(), [](std::string s) { return s.substr(s.find_last_of("/\\") + 1); });
    //print new files
    //replace the .dat with .root in goodFiles
    for (auto &filename : goodFiles) {
        size_t pos = filename.rfind(".dat");
        if (pos != std::string::npos) {
            filename.replace(pos, 4, ".root");
        }
    }
    std::transform(goodFiles2.begin(), goodFiles2.end(), goodFiles2.begin(), [](std::string s)
                   { return s.substr(s.find_last_of("/\\") + 1); });

    for (auto &filename : goodFiles2)
    {
        size_t pos = filename.rfind(".dat");
        if (pos != std::string::npos)
        {
            filename.replace(pos, 4, ".root");
        }
    }
    //now create the corresponding list of decoded files appending the entry in goodFiles to the decodedPath
    std::transform(goodFiles.begin(), goodFiles.end(), goodFiles.begin(), [&decodedPath, &runNumber](std::string s)
                   { return decodedPath + "run_" + runNumber + "/" + s; });
    std::transform(goodFiles2.begin(), goodFiles2.end(), goodFiles2.begin(), [&decodedPath, &runNumber2](std::string s)
                   { return decodedPath + "run_" + runNumber2 + "/" + s; });

    for (auto file : goodFiles)
    {
        std::cout << "-- " << file << std::endl;
    }
    for (auto file : goodFiles2)
    {
        std::cout << "-- " << file << std::endl;
    }


    auto treader1 = new TTreeReader();
    treader1->parseChain(goodFiles);
    treader1->loadEventNumber(0);
    uint32_t actualSuperID1 = treader1->SuperID->at(0);
    std::cout << "Starting loop - " << actualSuperID1 << " " << firstSuperID << std::endl;
    while (actualSuperID1 < firstSuperID){
        treader1->loadNextEvent();
        actualSuperID1 = treader1->SuperID->at(0);
    }

    auto treader2 = new TTreeReader();
    treader2->parseChain(goodFiles2);
    treader2->loadEventNumber(0);
    uint32_t actualSuperID2 = treader2->SuperID->at(0);
    std::cout << "Starting loop - " << actualSuperID2 << " " << firstSuperID << std::endl;
    while (actualSuperID2 < firstSuperID)
    {
        treader2->loadNextEvent();
        actualSuperID2 = treader2->SuperID->at(0);
    }

    //ok, now just load the superIDs in the container
    OutputTTreeContainer *outputContainer = new OutputTTreeContainer("../pippo.root");
    auto stubContainer = new StubContainer(outputContainer);
    //just load 10 superIDs
    std::cout << "Actual superID1 is : " << actualSuperID1 << std::endl;
    std::cout << "Actual superID2 is : " << actualSuperID2 << std::endl;
    std::cout << "Finishing at: " << firstSuperID + range << std::endl;
    //getchar();
    uint32_t tmpSuperID = firstSuperID;
    while (actualSuperID1 < firstSuperID + range || actualSuperID2 < firstSuperID + range)
    {
        tmpSuperID += 100;
        std::cout << "\rGoing to superID: " << tmpSuperID << std::flush;
        while (actualSuperID1 < tmpSuperID)
        {
            if(actualSuperID1 > firstSuperID + range) break;
            auto stubs = treader1->getStubsFromLoadedEvent();
            //getchar();
            for (auto &s : stubs)
            {
                stubContainer->addStub(s);
            }
            treader1->loadNextEvent();
            actualSuperID1 = treader1->SuperID->at(0);
        }
        while (actualSuperID2 < tmpSuperID)
        {
            if(actualSuperID2 > firstSuperID + range) break;
            auto stubs = treader2->getStubsFromLoadedEvent();
            // getchar();
            for (auto &s : stubs)
            {
                stubContainer->addStub(s);
            }
            treader2->loadNextEvent();
            actualSuperID2 = treader2->SuperID->at(0);
        }
        stubContainer->cleanStubsAndFillTree();
    }
    std::cout << std::endl;
    outputContainer->write();

    return 1;
} 