#include <TTree.h>
#include "FileManager.hpp"
#include "StubContainer.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <filesystem>
#include <algorithm>

int main(int argc, char *argv[]) {
    std::string outputFileName = "merged.root";
    const std::string directory_4017 = "/eos/experiment/mu-e/staging/daq/2023/decoded/commissioning/run_3224/";
    const std::string directory_4018 = "/eos/experiment/mu-e/staging/daq/2023/decoded/commissioning/run_3225/";
    std::cout << "Init fileManager" << std::endl;
    FileManager fileManager(directory_4017);
    std::cout << "Init ended" << std::endl;
    FileManager fileManager2(directory_4018);
    TTreeReader *reader = new TTreeReader();
    TTreeReader *reader2 = new TTreeReader();
    std::cout << "Parsing reader" << std::endl;
    fileManager.parseReader(reader);
    std::cout << "Parsed reader" << std::endl;
    fileManager2.parseReader(reader2);
    fileManager.searchForFirstSuperIDSameOrGreater(7614793 + 1);
    unsigned firstSuperID = 9263494;
    unsigned lastSuperID = firstSuperID + 2;

    OutputTTreeContainer *outputContainer = new OutputTTreeContainer("merged.root");
    StubContainer *sCont = new StubContainer(outputContainer);
    std::cout << "Binding stub container" << std::endl;
    fileManager.bindStubContainer(sCont);
    fileManager2.bindStubContainer(sCont);
    fileManager.cleanFiles();
    fileManager2.cleanFiles();
    //fileManager.searchForSuperIDRange(firstSuperID, lastSuperID);
    while(firstSuperID < lastSuperID) {
        //fileManager.searchForSuperID(firstSuperID);
        //fileManager2.searchForSuperID(firstSuperID);

        fileManager.loadSuperIDInContainer(firstSuperID);
        //fileManager2.loadSuperIDInContainer(firstSuperID);
        firstSuperID+=100;
        sCont->cleanStubsAndFillTree();
    }

    //sCont->cleanStubsAndFillTree();
    outputContainer->write();
    std::cout << "Tree written" << std::endl;
    delete outputContainer;
    std::cout << "deleted output" << std::endl;
    delete sCont;
    std::cout << "deleted scont" << std::endl;
    delete reader;
    //delete reader2;
    std::cout << "deleted reader" << std::endl;
    std::cout << "Deleted" << std::endl;
    //getchar();
    return 1;
}