#include <iostream>
#include <fstream>
#include <string>
#include <curl/curl.h>
#include <nlohmann/json.hpp>
#include <filesystem>
#include "parser.hpp"
#include "Querier.hpp"

using json = nlohmann::json;

uint32_t getAverageSuperIDsPerFile(const superIDRange& range) {
    if (range.numberOfFiles == 0) return 0;
    
    uint32_t totalSuperIDs = range.lastSuperID - range.firstSuperID + 1;
    return std::ceil(static_cast<float>(totalSuperIDs) / range.numberOfFiles);
}

int main(int argc, char *argv[])
{
    uint32_t runNumbers[2];
    std::string output;
    if(cmdOptionExists(argv, argv + argc, "-r1") && cmdOptionExists(argv, argv + argc, "-r2")) {
        runNumbers[0] = std::stoul(getCmdOption(argv, argv + argc, "-r1"));
        runNumbers[1] = std::stoul(getCmdOption(argv, argv + argc, "-r2"));
    } else {
        throw std::invalid_argument("Both runs should be specified with -r1 and -r2 options");
    }
    if(cmdOptionExists(argv, argv + argc, "-o")) {
        output = getCmdOption(argv, argv + argc, "-o");
    } else {
        output = "jsonSetupAllRun.json";
    }

    std::string rawPath = "/eos/experiment/mu-e/staging/daq/2023/raw/commissioning/";
    Querier *querier = new Querier();

    superIDRange combinedRange;
    combinedRange.firstSuperID = std::numeric_limits<uint32_t>::max();
    combinedRange.lastSuperID = 0;
    combinedRange.numberOfFiles = 0;

    for(int i = 0; i < 2; ++i) {
        querier->setUrl("http://muedaq-supervisor/api/v1/data/raw/" + std::to_string(runNumbers[i]) + "/query");
        querier->setFolder(rawPath + "run_" + std::to_string(runNumbers[i]) + "/");
        std::cout << "Getting superID range for run " << runNumbers[i] << std::endl;
        auto sIdRange = querier->getSuperIDRange();
        std::cout << "First superID: " << sIdRange.firstSuperID << std::endl;
        std::cout << "Last superID: " << sIdRange.lastSuperID << std::endl;

        combinedRange.firstSuperID = std::min(combinedRange.firstSuperID, sIdRange.firstSuperID);
        combinedRange.lastSuperID = std::max(combinedRange.lastSuperID, sIdRange.lastSuperID);
        combinedRange.numberOfFiles = std::max(combinedRange.numberOfFiles, sIdRange.numberOfFiles);
        combinedRange.superIDsPerFile = std::max(combinedRange.superIDsPerFile, sIdRange.superIDsPerFile);
    }


    // Output combined results...
    std::cout << "First superID: " << combinedRange.firstSuperID << std::endl;
    std::cout << "Last superID: " << combinedRange.lastSuperID << std::endl;
    std::cout << "Number of files: " << combinedRange.numberOfFiles << std::endl;
    std::cout << "SuperIDs per file: " << combinedRange.superIDsPerFile << std::endl;

    json fileListJson;

    for (uint32_t currentID = combinedRange.firstSuperID; currentID <= combinedRange.lastSuperID; currentID += combinedRange.superIDsPerFile) {
        std::cout << "\rAt superID: " << currentID << std::flush;
        uint32_t upperBound = currentID + combinedRange.superIDsPerFile - 1;
        
        json entry;
        entry["start"] = currentID;
        entry["end"] = std::min(upperBound, combinedRange.lastSuperID);
        entry["run_1"] = std::to_string(runNumbers[0]);
        entry["run_2"] = std::to_string(runNumbers[1]);

        for (int i = 0; i < 2; ++i) {
            querier->setUrl("http://muedaq-supervisor/api/v1/data/raw/" + std::to_string(runNumbers[i]) + "/query");
            querier->setFolder(rawPath + "run_" + std::to_string(runNumbers[i]) + "/");
            auto files = querier->getFilesInRange(currentID, upperBound - currentID + 1);
            entry["run_" + std::to_string(runNumbers[i])] = files;
        }

        fileListJson.push_back(entry);
    }
    std::cout << std::endl;

    // To save it to a file or print it
    std::ofstream jsonFile(output.c_str());
    jsonFile << fileListJson.dump(4);
}
