#include <TTree.h>
#include "FileManager.hpp"
#include "StubContainer.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <filesystem>
#include <algorithm>
#include <chrono>


int main(int argc, char *argv[]) {
    // get time
    std::string outputFileName = "merged.root";
    const std::string directory_4017 = "/eos/experiment/mu-e/staging/daq/2023/decoded/commissioning/run_3224/";
    FileManager fileManager(directory_4017);
    std::cout << "FileManager init " << std::endl;
    TTreeReader *reader = new TTreeReader();
    std::cout << "Reader init " << std::endl;
    fileManager.parseReader(reader);
    std::cout << "Reader parsed " << std::endl;
    unsigned firstSuperID = 9263494;
    unsigned lastSuperID = firstSuperID + 10;
    OutputTTreeContainer *outputContainer = new OutputTTreeContainer("merged.root");
    StubContainer *sCont = new StubContainer(outputContainer);
    fileManager.bindStubContainer(sCont);
    std::cout << "Binded stub container" << std::endl;

    std::cout << "Search for superID fastly: " << firstSuperID << std::endl;
    auto start = std::chrono::high_resolution_clock::now();
    fileManager.searchForSuperID(firstSuperID);
    auto end = std::chrono::high_resolution_clock::now();
    std::cout << "It took: " << std::chrono::duration_cast<std::chrono::seconds>(end - start).count() << " s" << std::endl;
    std::cout << "----" << std::endl;
    std::cout << "Search for superID slowly: " << firstSuperID << std::endl;
    start = std::chrono::high_resolution_clock::now();
    fileManager.searchForSuperID(firstSuperID, false);
    end = std::chrono::high_resolution_clock::now();
    std::cout << "It took: " << std::chrono::duration_cast<std::chrono::seconds>(end - start).count() << " s" << std::endl;

    outputContainer->write();
    std::cout << "Tree written" << std::endl;
    delete outputContainer;
    std::cout << "deleted output" << std::endl;
    delete sCont;
    std::cout << "deleted scont" << std::endl;
    delete reader;
    std::cout << "deleted reader" << std::endl;
    std::cout << "Deleted" << std::endl;
    //getchar();
    return 1;
}