#include <iostream>
#include <fstream>
#include <string>
#include <curl/curl.h>
#include <nlohmann/json.hpp>
#include <filesystem>
#include "parser.hpp"
#include "Querier.hpp"

using json = nlohmann::json;

int main(int argc, char *argv[])
{
    //parse option -f for firstsuperID
    //parse option -r for range
    //parse option -o for output file
    uint32_t firstSuperID;
    std::string outputFileName = "merged.txt";
    uint32_t range = 100000;
    uint32_t runNumber;
    
    if (cmdOptionExists(argv, argv + argc, "--superID")) {
        //convert option to from string to uint
        firstSuperID = std::stoul(getCmdOption(argv, argv + argc, "--superID"));
    } else {
        throw std::invalid_argument("No superID specified, please specify it with --superID option");
    }
    if (cmdOptionExists(argv, argv + argc, "--range")) {
        // convert option to from string to uint
        range = std::stoul(getCmdOption(argv, argv + argc, "--range"));
    } else {
        std::cout << "No range specified, using default: 100000" << std::endl;
    }
    if (cmdOptionExists(argv, argv + argc, "-o")) {
        outputFileName = getCmdOption(argv, argv + argc, "-o");
        std::cout << "Output file: " << outputFileName << std::endl;
    } else {
        std::cout << "No output file specified, using default: merged.txt" << std::endl;
    }
    if(cmdOptionExists(argv, argv + argc, "-r")) {
        runNumber = std::stoul(getCmdOption(argv, argv + argc, "-r"));
        std::cout << "Run number: " << runNumber << std::endl;
    } else {
        throw std::invalid_argument("No run specified, please specify it with -r option");
    }

    std::string rawPath = "/eos/experiment/mu-e/staging/daq/2023/raw/commissioning/";
    Querier *querier = new Querier();
    querier->setUrl("http://muedaq-supervisor/api/v1/data/raw/" + std::to_string(runNumber) + "/query");
    querier->setFolder(rawPath + "run_" + std::to_string(runNumber) + "/");
    //querier->printRanges();
    std::ofstream outFile(outputFileName);
    auto goodFiles = querier->getFilesInRange(firstSuperID, range);
    for(auto& f: goodFiles){
        outFile << f << std::endl;
    }
    outFile.close();
}