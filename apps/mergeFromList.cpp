#include <TTree.h>
#include "FileManager.hpp"
#include "Querier.hpp"
#include "StubContainer.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <filesystem>
#include <algorithm>
#include "parser.hpp"
#include <time.h>

std::vector<std::string> transformFileNames(const std::vector<std::string> &originalFiles, const std::string &decodedPath) {
    std::vector<std::string> transformedFiles;
    for (const auto &filename : originalFiles) {
        std::string runFolder = filename.substr(filename.find("run_"), filename.find("/", filename.find("run_")) - filename.find("run_"));
        std::string fileOnly = filename.substr(filename.find_last_of("/\\") + 1);
        size_t pos = fileOnly.rfind(".dat");
        if (pos != std::string::npos) {
            fileOnly.replace(pos, 4, ".root");
        }
        transformedFiles.push_back(decodedPath + runFolder + "/" + fileOnly);
    }
    return transformedFiles;
}

int main(int argc, char *argv[]) {

    std::string list1Name, list2Name, outputFileName;
    unsigned firstSuperID, range;

    if (cmdOptionExists(argv, argv + argc, "--list1")) {
        list1Name = getCmdOption(argv, argv + argc, "--list1");
        std::cout << "First list: " << list1Name << std::endl;
    } else {
        std::cout << "No first list specified, exiting." << std::endl;
        return 1; // or a suitable error code
    }

    if (cmdOptionExists(argv, argv + argc, "--list2")) {
        list2Name = getCmdOption(argv, argv + argc, "--list2");
        std::cout << "Second list: " << list2Name << std::endl;
    } else {
        std::cout << "No second list specified, exiting." << std::endl;
        return 1; // or a suitable error code
    }

    if (cmdOptionExists(argv, argv + argc, "--firstSuperID"))
    {
        firstSuperID = std::stoul(getCmdOption(argv, argv + argc, "--firstSuperID"));
        std::cout << "First SuperID: " << firstSuperID << std::endl;
    } else {
        std::cout << "No firstSuperID specified, exiting" << std::endl;
        return 1;
    }

    if (cmdOptionExists(argv, argv + argc, "--range")) {
        range = std::stoul(getCmdOption(argv, argv + argc, "--range"));
        std::cout << "Range: " << range << std::endl;
    } else {
        std::cout << "No range specified, using default: 100000" << std::endl;
        range = 100000;
    }

    if (cmdOptionExists(argv, argv + argc, "-o")) {
        outputFileName = getCmdOption(argv, argv + argc, "-o");
        std::cout << "Output file: " << outputFileName << std::endl;
    } else {
        std::cout << "No output file specified, using default: merged.root" << std::endl;
        outputFileName = "merged.root";
    }

    std::ifstream inFile1(list1Name);
    std::ifstream inFile2(list2Name);
    std::string line;
    std::vector<std::string> goodFiles;
    std::vector<std::string> goodFiles2;

    while (std::getline(inFile1, line)) {
        goodFiles.push_back(line);
    }
    while (std::getline(inFile2, line)) {
        goodFiles2.push_back(line);
    }
    inFile1.close();
    inFile2.close();

    std::string decodedPath = "/muedaq_merge/";
    std::vector<std::string> transformedFiles;
    std::vector<std::string> transformedFiles2;

    goodFiles = transformFileNames(goodFiles, decodedPath);
    goodFiles2 = transformFileNames(goodFiles2, decodedPath);

    auto treader1 = new TTreeReader();
    treader1->parseChain(goodFiles);
    treader1->loadEventNumber(0);
    uint32_t actualSuperID1 = treader1->SuperID->at(0);
    std::cout << "Starting loop - " << actualSuperID1 << " " << firstSuperID << std::endl;
    while (actualSuperID1 < firstSuperID)
    {
        treader1->loadNextEvent();
        actualSuperID1 = treader1->SuperID->at(0);
    }

    auto treader2 = new TTreeReader();
    treader2->parseChain(goodFiles2);
    treader2->loadEventNumber(0);
    uint32_t actualSuperID2 = treader2->SuperID->at(0);
    std::cout << "Starting loop - " << actualSuperID2 << " " << firstSuperID << std::endl;
    while (actualSuperID2 < firstSuperID)
    {
        treader2->loadNextEvent();
        actualSuperID2 = treader2->SuperID->at(0);
    }

    // ok, now just load the superIDs in the container
    OutputTTreeContainer *outputContainer = new OutputTTreeContainer(outputFileName.c_str());
    auto stubContainer = new StubContainer(outputContainer);
    std::cout << "Finishing at: " << firstSuperID + range << std::endl;
    uint32_t tmpSuperID = firstSuperID;
    while (actualSuperID1 < firstSuperID + range || actualSuperID2 < firstSuperID + range)
    {
        tmpSuperID += 100;
        std::cout << "\rGoing to superID: " << tmpSuperID << std::flush;
        while (actualSuperID1 < tmpSuperID)
        {
            if (actualSuperID1 > firstSuperID + range)
                break;
            auto stubs = treader1->getStubsFromLoadedEvent();
            for (auto &s : stubs)
            {
                stubContainer->addStub(s);
            }
            treader1->loadNextEvent();
            actualSuperID1 = treader1->SuperID->at(0);
        }
        while (actualSuperID2 < tmpSuperID)
        {
            if (actualSuperID2 > firstSuperID + range)
                break;
            auto stubs = treader2->getStubsFromLoadedEvent();
            for (auto &s : stubs)
            {
                stubContainer->addStub(s);
            }
            treader2->loadNextEvent();
            actualSuperID2 = treader2->SuperID->at(0);
        }
        stubContainer->cleanStubsAndFillTree();
    }
    std::cout << std::endl;
    outputContainer->write();

    return 1;
} 