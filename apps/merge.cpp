#include <TTree.h>
#include "FileManager.hpp"
#include "StubContainer.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <filesystem>
#include <algorithm>
#include "parser.hpp"
#include <time.h>

int main(int argc, char *argv[])
{
    std::string outputFileName;
    std::string firstRunDirectory;
    std::string secondRunDirectory;
    unsigned firstSuperID;
    int range = 10;

    if (cmdOptionExists(argv, argv + argc, "-o")) {
        outputFileName = getCmdOption(argv, argv + argc, "-o");
        std::cout << "Output file: " << outputFileName << std::endl;
    } else {
        std::cout << "No output file specified, using default: merged.root" << std::endl;
        outputFileName = "merged.root";
    }
    if (cmdOptionExists(argv, argv + argc, "--firstRun")) {
        firstRunDirectory = getCmdOption(argv, argv + argc, "--firstRun");
        std::cout << "First run directory: " << firstRunDirectory << std::endl;
    } else {
        std::cout << "No first run directory specified, exiting" << std::endl;
        return 0;
    }
    if (cmdOptionExists(argv, argv + argc, "--secondRun")) {
        secondRunDirectory = getCmdOption(argv, argv + argc, "--secondRun");
        std::cout << "Second run directory: " << secondRunDirectory << std::endl;
    } else {
        std::cout << "No second run directory specified, exiting" << std::endl;
        return 0;
    }
    if (cmdOptionExists(argv, argv + argc, "--range")) {
        range = std::stoi(getCmdOption(argv, argv + argc, "--range"));
    } else {
        std::cout << "No range specified, using default: 10" << std::endl;
        std::cout << "Range: " << range << std::endl;
    }
    if (cmdOptionExists(argv, argv + argc, "--firstSuperID")) {
        firstSuperID = std::stoi(getCmdOption(argv, argv + argc, "--firstSuperID"));
        std::cout << "First superID: " << firstSuperID << std::endl;
    } else {
        std::cout << "No first superID specified, exiting" << std::endl;
        return 0;
    }
    
    std::cout << "First run directory: " << firstRunDirectory << std::endl;
    FileManager fileManager(firstRunDirectory);
    FileManager fileManager2(secondRunDirectory);
    TTreeReader *reader = new TTreeReader();
    TTreeReader *reader2 = new TTreeReader();
    fileManager.parseReader(reader);
    fileManager2.parseReader(reader2);
    unsigned lastSuperID = firstSuperID + range;

    OutputTTreeContainer *outputContainer = new OutputTTreeContainer(outputFileName.c_str());
    StubContainer *sCont = new StubContainer(outputContainer);
    std::cout << "Binding stub container" << std::endl;
    fileManager.bindStubContainer(sCont);
    fileManager2.bindStubContainer(sCont);
    std::cout << "Start superID loading " << std::endl;
    while (firstSuperID < lastSuperID)
    {
        //fileManager.searchForSuperID(firstSuperID);
        //fileManager2.searchForSuperID(firstSuperID);

        fileManager.loadSuperIDInContainer(firstSuperID);
        fileManager2.loadSuperIDInContainer(firstSuperID);
        firstSuperID+=100;
        sCont->cleanStubsAndFillTree();
        std::cout << "Loaded first 100 superIDs" << std::endl;
    }

    outputContainer->write();
    std::cout << "Tree written" << std::endl;
    delete outputContainer;
    std::cout << "deleted output" << std::endl;
    delete sCont;
    std::cout << "deleted scont" << std::endl;
    delete reader;
    delete reader2;
    std::cout << "deleted reader" << std::endl;
    std::cout << "Deleted" << std::endl;
    // getchar();
    return 1;
    }