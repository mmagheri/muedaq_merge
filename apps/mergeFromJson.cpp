#include <TTree.h>
#include "FileManager.hpp"
#include "Querier.hpp"
#include "StubContainer.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <filesystem>
#include <algorithm>
#include "parser.hpp"
#include <time.h>
#include <nlohmann/json.hpp>

const int NITERATIONS_TOSKIP = 1;

std::vector<std::string> transformFileNames(const std::vector<std::string> &originalFiles, const std::string &decodedPath) {
    std::vector<std::string> transformedFiles;
    for (const auto &filename : originalFiles) {
        std::string runFolder = filename.substr(filename.find("run_"), filename.find("/", filename.find("run_")) - filename.find("run_"));
        std::string fileOnly = filename.substr(filename.find_last_of("/\\") + 1);
        size_t pos = fileOnly.rfind(".dat");
        if (pos != std::string::npos) {
            fileOnly.replace(pos, 4, ".root");
        }
        transformedFiles.push_back(decodedPath + runFolder + "/" + fileOnly);
    }
    return transformedFiles;
}

int main(int argc, char *argv[]) {

    std::string jsonSetup, outputFileName;
    unsigned firstSuperID, range;
    std::string decodedPath = "/eos/experiment/mu-e/staging/daq/2023/decoded/commissioning/";
    bool isVcth = false;

    if (cmdOptionExists(argv, argv + argc, "-j")) {
        jsonSetup = getCmdOption(argv, argv + argc, "-j");
        std::cout << "json setup: " << jsonSetup << std::endl;
    } else {
        std::cout << "No first list specified, exiting." << std::endl;
        return 1; // or a suitable error code
    }

    if (cmdOptionExists(argv, argv + argc, "-o")) {
        outputFileName = getCmdOption(argv, argv + argc, "-o");
        std::cout << "Output file: " << outputFileName << std::endl;
    } else {
        std::cout << "No output file specified, using default: merged.root" << std::endl;
        outputFileName = "merged.root";
    }

    if (cmdOptionExists(argv, argv + argc, "--decodedPath")) {
        decodedPath = getCmdOption(argv, argv + argc, "--decodedPath");
        std::cout << "Decoded files path: " << decodedPath << std::endl;
    } 

    std::ifstream inFile(jsonSetup);
    nlohmann::json j;
    inFile >> j;
    inFile.close();
    if (j.contains("start") && j["start"].is_number_unsigned()) {
    firstSuperID = j["start"].get<unsigned>();
    std::cout << "Start (First SuperID): " << firstSuperID << std::endl;
    } else {
        std::cout << "No start specified in JSON, exiting" << std::endl;
        return 1;
    }

    if (j.contains("end") && j["end"].is_number_unsigned()) {
        unsigned endSuperID = j["end"].get<unsigned>();
        range = endSuperID - firstSuperID;
        std::cout << "End SuperID: " << endSuperID << std::endl;
        std::cout << "Calculated Range: " << range << std::endl;
    } else {
        std::cout << "No end specified in JSON, exiting" << std::endl;
        return 1;
    }

    std::vector<std::string> goodFiles1;
    std::vector<std::string> goodFiles2;

    std::string run1, run2;
    if(j.contains("run_1") && j.contains("run_2")) {
        run1 = j["run_1"].get<std::string>();
        run2 = j["run_2"].get<std::string>();
    } else {
        std::cout << "Run names to merge not found in JSON, exiting" << std::endl;
        return 1;
    }

    if (j.contains("run_" + run1)) {
        for (const auto& file : j["run_" + run1]) {
            goodFiles1.push_back(file.get<std::string>());
        }
    }

    if (j.contains("run_" + run2)) {
        for (const auto& file : j["run_" + run2]) {
            goodFiles2.push_back(file.get<std::string>());
        }
    }


    goodFiles1 = transformFileNames(goodFiles1, decodedPath);
    goodFiles2 = transformFileNames(goodFiles2, decodedPath);
    std::cout << "good files:" << std::endl;
    std::cout << "Run1 - " << std::endl;
    for(auto& file : goodFiles1) {
        std::cout << file << std::endl;
    }
    std::cout << "Run2 - " << std::endl;
    
    for(auto& file : goodFiles2) {
        std::cout << file << std::endl;
    }

    if(goodFiles2.size() == 0 && goodFiles1.size() == 0) {
        std::cout << "No files found, exiting" << std::endl;
        return 1;
    }
    //getchar();


    bool run1Finished = false;
    bool run2Finished = false;
    std::cout << "Doing treereader1" << std::endl;
    auto treader1 = new TTreeReader();
    treader1->setVcthScan(isVcth);
    uint32_t actualSuperID1 = 0;
    if(goodFiles1.size()!=0) {
        treader1->parseChain(goodFiles1);
        treader1->loadEventNumber(0);
        actualSuperID1 = treader1->getLoadedSuperID();// treader1->SuperID->at(0);
        std::cout << "Starting loop - " << actualSuperID1 << " " << firstSuperID << std::endl;
        while (actualSuperID1 < firstSuperID) {
            run1Finished = !treader1->loadNextEvent();
            if(run1Finished) break;
            actualSuperID1 = treader1->SuperID->at(0);
        }
    }
    std::cout << "Doing treereader2" << std::endl;
    auto treader2 = new TTreeReader();
    treader2->setVcthScan(isVcth);
    uint32_t actualSuperID2 = 0;
    if(goodFiles2.size()!=0) {
        treader2->parseChain(goodFiles2);
        treader2->loadEventNumber(0);
        actualSuperID2 = treader2->getLoadedSuperID(); //->at(0);
        std::cout << "Starting loop - " << actualSuperID2 << " " << firstSuperID << std::endl;
        while (actualSuperID2 < firstSuperID) {
            run2Finished = !treader2->loadNextEvent();
            if(run2Finished) break;
            actualSuperID2 = treader2->SuperID->at(0);
        }
    }

    // ok, now just load the superIDs in the container
    std::cout << "init outputcontainer"<< std::endl;
    auto *outputContainer = new OutputTTreeContainer(outputFileName.c_str());
    outputContainer->setVcthScan(isVcth);
    std::cout << "init stubcontainer" << std::endl;
    auto stubContainer = new StubContainer(outputContainer);
    std::cout << "Finishing at: " << firstSuperID + range << std::endl;
    uint32_t tmpSuperID = firstSuperID;
    bool lastSuperID = false;
    while ((actualSuperID1 <= firstSuperID + range || actualSuperID2 <= firstSuperID + range) && (!lastSuperID) && !(run1Finished && run2Finished))
    {
        tmpSuperID += 5;
        if(tmpSuperID > firstSuperID + range) {
            tmpSuperID = firstSuperID + range;
            lastSuperID = true;
            std::cout << std::endl << "LAST SUPERID: " << tmpSuperID << std::endl;

        }
        std::cout << "\rGoing to superID: " << tmpSuperID << std::flush;
        std::cout << "\nFilling cont 1 " << std::endl;
        while (actualSuperID1 <= tmpSuperID && goodFiles1.size()!= 0 && !run1Finished) {
            if (actualSuperID1 > firstSuperID + range) {
                run1Finished = true;
                break;
            }
            auto stubs = treader1->getStubsFromLoadedEvent();
            
            for (auto &s : stubs) {
                stubContainer->addStub(s);
            }
            //std::cout << "RUN1" << std::endl;
            run1Finished = !treader1->loadNextEvent();
            if(!run1Finished) {
                actualSuperID1 = treader1->SuperID->at(0);
            }
        }
        std::cout << "\nFilling cont 2 " << std::endl;

        while (actualSuperID2 <= tmpSuperID && goodFiles2.size()!= 0 && !run2Finished) {
            if (actualSuperID2 > firstSuperID + range) {
                run2Finished = true;
                break;
            }
            std::cout << "checked superID" << std::endl; 
            auto stubs = treader2->getStubsFromLoadedEvent();
            std::cout << "got: " << stubs.size() << " stubs" << std::endl; 

            for (auto &s : stubs) {
                stubContainer->addStub(s);
            }
            std::cout << "added stubs to the container" << std::endl; 

            //std::cout << "RUN2" << std::endl;
            run2Finished = !treader2->loadNextEvent();
            std::cout << "loaded next event" << std::endl; 

            if(!run2Finished) {
                actualSuperID2 = treader2->SuperID->at(0);
            }
            std::cout << "Updated superID" << std::endl; 

        }
        
        std::cout << "\n" << "Cleaning and fillin tree\n";
        stubContainer->cleanStubsAndFillTree();
        std::cout << "\n" << "Cleaned and filled tree\n";


        //std::cout << "Press a key to go the the next iteration" << std::endl;
        //getchar();


    }
    std::cout << std::endl;
    std::cout << "Writing tree\n";
    outputContainer->write();
    std::cout << "Closing tree\n";
    delete outputContainer;
    delete treader1;
    delete treader2;
    delete stubContainer;
    return 0;
} 

