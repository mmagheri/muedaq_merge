#include "Querier.hpp"
#include <curl/curl.h>
#include <stdexcept>
#include <iostream>

size_t Querier::WriteCallback(void *contents, size_t size, size_t nmemb, std::string *output) {
    size_t totalSize = size * nmemb;
    output->append(static_cast<char *>(contents), totalSize);
    return totalSize;
}

Querier::Querier() {
    url = "http://muedaq-supervisor/api/v1/data/raw/4018/query";
    requestBody["query"]["path"] = "/eos/experiment/mu-e/staging/daq/2023/raw/commissioning/run_4018/4018_01444f263_014453262_4200000.dat";
}

void Querier::setRequestBody(const json& newRequestBody) {
    requestBody = newRequestBody;
}

void Querier::setRequestBody(const std::string& newRequestBody) {
    requestBody["query"]["path"] = newRequestBody;
}

void Querier::setFolder(const std::string &newFolder) {
    folder = newFolder;
}

void Querier::setUrl(const std::string &newUrl) {
    url = newUrl;
}

json Querier::makeRequest() {
    CURL *curl = curl_easy_init();
    if (!curl)
    {
        throw std::runtime_error("Failed to initialize cURL.");
    }

    std::string requestBodyStr = requestBody.dump();
    std::string responseData;

    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_POST, 1L);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, requestBodyStr.c_str());
    curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, requestBodyStr.size());
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &responseData);

    struct curl_slist *headers = nullptr;
    headers = curl_slist_append(headers, "Content-Type: application/json");
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

    CURLcode res = curl_easy_perform(curl);
    
    if (res != CURLE_OK)
    {
        curl_easy_cleanup(curl);
        throw std::runtime_error("cURL request failed");
    }

    curl_easy_cleanup(curl);
    curl_slist_free_all(headers);

    return json::parse(responseData);
}

std::vector<std::string> Querier::getFilesInRange(unsigned int targetSuperID, unsigned int range) {
    std::vector<std::string> goodFiles;
    for (const auto & entry : std::filesystem::directory_iterator(folder)){
        setRequestBody(entry.path().string());
        auto output = makeRequest();
        //std::cout << output["data"][0]["superIDs"]["first"] << std::endl;
        //std::cout << output["data"][0]["superIDs"]["last"] << std::endl;
        if ((output["data"][0]["superIDs"]["last"] >= targetSuperID && output["data"][0]["superIDs"]["first"] <= targetSuperID + range) ||
            (output["data"][0]["superIDs"]["first"] <= targetSuperID + range && output["data"][0]["superIDs"]["last"] >= targetSuperID)) {
            goodFiles.push_back(entry.path().string());
            //std::cout << "------ good file: " << std::endl;
            //std::cout << entry.path().string() << std::endl;
            //std::cout << output["data"][0]["superIDs"]["first"] << std::endl;
            //std::cout << output["data"][0]["superIDs"]["last"] << std::endl;
        }
    }
    
    return goodFiles;
}

void Querier::printRanges()
{
    for (const auto &entry : std::filesystem::directory_iterator(folder))
    {
        setRequestBody(entry.path().string());
        auto output = makeRequest();
        // std::cout << output["data"][0]["superIDs"]["first"] << std::endl;
        // std::cout << output["data"][0]["superIDs"]["last"] << std::endl;
        std::cout << "------ good file: " << std::endl;
        std::cout << entry.path().string() << std::endl;
        std::cout << output["data"][0]["superIDs"]["first"] << std::endl;
        std::cout << output["data"][0]["superIDs"]["last"] << std::endl;
    }

}

json Querier::getRunInfo(std::uint32_t runNumber) {
    setUrl("http://muedaq-supervisor/api/v1/data/raw/" + std::to_string(runNumber) + "/query");
}

superIDRange Querier::getSuperIDRange() {
    superIDRange result;
    result.firstSuperID = std::numeric_limits<uint32_t>::max();
    result.lastSuperID = 0;
    result.numberOfFiles = 0;

    for (const auto & entry : std::filesystem::directory_iterator(folder)){
        result.numberOfFiles++;
        setRequestBody(entry.path().string());

        try {
            auto output = makeRequest();
            uint32_t first = output["data"][0]["superIDs"]["first"];
            uint32_t last = output["data"][0]["superIDs"]["last"];
            result.firstSuperID = std::min(result.firstSuperID, first);
            result.lastSuperID = std::max(result.lastSuperID, last);
        } catch (const nlohmann::json::exception& e) {
            // Log error and continue to next file
            std::cerr << "Error parsing JSON: " << e.what() << std::endl;
            continue;
        } catch (const std::exception& e) {
            // Log any other standard exceptions and continue
            std::cerr << "Error: " << e.what() << std::endl;
            continue;
        }
    }
    uint32_t totalSuperIDs = result.lastSuperID - result.firstSuperID + 1;
    result.superIDsPerFile = std::ceil(static_cast<float>(totalSuperIDs) / result.numberOfFiles);

    return result;
}
