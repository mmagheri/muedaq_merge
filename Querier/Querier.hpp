#pragma once
#include <string>
#include <nlohmann/json.hpp>

using json = nlohmann::json;

struct superIDRange {
    uint32_t firstSuperID;
    uint32_t lastSuperID;
    uint32_t numberOfFiles;
    uint32_t superIDsPerFile;
};

class Querier
{
private:
    static size_t WriteCallback(void *contents, size_t size, size_t nmemb, std::string *output);
    std::string url;
    json requestBody;
    std::string folder;
public:
    Querier();
    void setRequestBody(const json& newRequestBody);
    void setRequestBody(const std::string& newRequestBody);
    void setUrl(const std::string &newUrl);
    void setFolder(const std::string &newFolder);

    json makeRequest();
    json getRunInfo(uint32_t runNumber);
    std::vector<std::string> getFilesInRange(unsigned int targetSuperID, unsigned int range);
    superIDRange getSuperIDRange();
    void printRanges();
};

