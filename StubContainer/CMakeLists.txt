add_library(
  StubContainer
  StubContainer.cpp
  OutputTTreeContainer.cpp
  Stub.cpp
)

find_package(ROOT 6.16 CONFIG REQUIRED)

target_include_directories(
    StubContainer PUBLIC
    ${ROOT_INCLUDE_DIRS}
)

target_link_libraries(
    StubContainer PUBLIC
    ${ROOT_LIBRARIES}
)