#include "Stub.hpp"

int Stub::isNBxDistantFrom(Stub secondStub)
{
    if(superID == secondStub.getSuperID()){
        return static_cast<int>(secondStub.getBx() - bx);
    }
    else
    {
        int deltaSuperIdInBx = static_cast<int>(secondStub.getSuperID() - superID) * NUMBER_OF_MAX_BX_IN_SUPERID;
        int deltaBx = static_cast<int>(secondStub.getBx()) - static_cast<int>(bx);
        return deltaSuperIdInBx + deltaBx;
    }
}

bool Stub::isInTheSameLocationAs(Stub secondStub) {
    if(secondStub.getLocalX() == localX && secondStub.getLocalY() == localY && secondStub.getLink() == link) {
        return true;
    }
    return false;
}

bool Stub::isBefore(Stub secondStub) {
    if(superID < secondStub.getSuperID()) return true;
    else if(superID == secondStub.getSuperID() && bx < secondStub.getBx()) return true;
    else return false;
}

bool Stub::isAfter(Stub secondStub) {
    return !(isBefore(secondStub));
}

void Stub::print() {
    std::cout << bx << " " << superID << "\n";
}

uint64_t Stub::getTimeStamp() {
    return (static_cast<uint64_t>(superID)*NUMBER_OF_MAX_BX_IN_SUPERID + static_cast<uint64_t>(bx)); 
}

void Stub::isPresentInASubsequentBx() {
    persistency++;
}

void Stub::clearPersistency() {
    persistency = 0;
}
