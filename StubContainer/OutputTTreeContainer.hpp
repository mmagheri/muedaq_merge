#pragma once

#include <vector>
#include <string>
#include <stdint.h>
#include "Stub.hpp"

///////////////
// ROOT HEADERS
#include <TROOT.h>
#include <TTree.h>
#include <TFile.h>
#include <TList.h>
#include <TObjString.h>

class OutputTTreeContainer
{
private:
    TFile *myFile;

    std::vector<uint16_t> *stub_Persistency = new std::vector<uint16_t>;
    std::vector<uint16_t> *stub_Bx = new std::vector<uint16_t>;
    std::vector<uint16_t> *stub_Link = new std::vector<uint16_t>;
    std::vector<uint32_t> *stub_SuperID = new std::vector<uint32_t>;
    std::vector<float> *stub_Bend = new std::vector<float>;
    std::vector<float> *stub_LocalX = new std::vector<float>;
    std::vector<float> *stub_LocalY = new std::vector<float>;

    std::vector<uint32_t> *stub_UserBits = new std::vector<uint32_t>;
    std::vector<uint16_t> *stub_ModuleID = new std::vector<uint16_t>;
    std::vector<uint16_t> *stub_Threshold = new std::vector<uint16_t>;

    bool isVcthScan = false;

public:
    TTree *eventTree;
    OutputTTreeContainer(std::string filename);
    OutputTTreeContainer();
    ~OutputTTreeContainer();
    void clearVectors();
    void write();
    void fill();
    void printNEntries();

    void pushSuperID(uint32_t superid);
    void pushBx(uint16_t bx);
    void pushBend(float bend);
    void pushLink(uint16_t link);
    void pushPersistency(uint16_t persistency);
    void pushLocalX(float localx);
    void pushLocalY(float localy);

    void setVcthScan(bool scan_);
    void pushUserBits(uint32_t userBits_);
    void pushModuleID(uint32_t moduleID_);
    void pushThreshold(uint32_t threshold_);

};