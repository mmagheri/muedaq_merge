#include "StubContainer.hpp"


StubContainer::StubContainer(OutputTTreeContainer* handler_) {
    std::cout << "assigning handler" << std::endl;
    handler = handler_;
    std::cout << "assigned handler" << std::endl;

    st = std::chrono::steady_clock::now();
    stubIndex = 0;
    std::cout << "did init" << std::endl;

}

StubContainer::StubContainer() {
    st = std::chrono::steady_clock::now();
} 

bool compareInterval(Stub stub1, Stub stub2) {
    return stub1.isBefore(stub2);
}

void StubContainer::addStub(Stub theStub) {
    if (stubIndex < NMAX_STUB_IN_CONTAINER) {
        stubs[stubIndex] = theStub;
        stubIndex ++;
    }
    else {
        //throw an error
        std::cout << "BAD NUMBER OF STUBS\n";
        throw std::invalid_argument("Too many stubs!");
    }
}

void StubContainer::printStubTiming() {
    std::cout << "--- Printing stubs timing\n";
    for(unsigned int i = 0; i < stubIndex; i++) {
        std::cout << "----- " << i << " ---- " << (stubs.at(i)).getSuperID() << " " << (stubs.at(i)).getBx() << "\n";
    }
    std::cout << "--- End printing stubs timing\n";
}

void StubContainer::printCleanStubTiming() {
    std::cout << "--- Printing stubs timing\n";
    for(auto&s : cleanedStubs) {
        std::cout << s.getSuperID() << " " << s.getBx() << "\n";
    }
    std::cout << "We got: " << cleanedStubs.size() << " good stubs\n";
    std::cout << "--- End printing stubs timing\n";
}

void StubContainer::cleanStubsAndFillTree() {
    std::sort(stubs.begin(), stubs.begin() + stubIndex, compareInterval);
    //std::cout << "cleaned stubs " << std::endl;
    removePersistencyAndBadStubs();
    fillTree();
    reset();
    nCycle++;
    if(nCycle%10000 == 0)std::cout << "\nCycle -- " << nCycle << "\n";
    if(nCycle%50000 == 0) {
        std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
        std::cout << "It took: " << std::chrono::duration_cast<std::chrono::seconds>(end - st).count() << "[seconds]" << std::endl;
    }
}

void StubContainer::removePersistencyAndBadStubs() {
    if(stubIndex<2) return;

    for(unsigned int i = 0; i<NMAX_STUB_IN_CONTAINER; i++) {
        stubIsBad[i] = false;
    }
    //for(unsigned int i = 0; i < stubIndex - 1; i++) {
    //    unsigned int secondStubPosition = i + 1;
    //    //std::cout << secondStubPosition << " --- "<< stubIndex << "\n";
    //    while((stubs.at(i)).isNBxDistantFrom(stubs.at(secondStubPosition)) < NUMBER_OF_BX_PERSISTENCY && stubIsBad[secondStubPosition] == false) {
    //        if ((stubs.at(i)).isInTheSameLocationAs(stubs.at(secondStubPosition))){
    //            std::cout << "HEY, THERE'S A BAD STUB!\n";
    //            stubIsBad[secondStubPosition] = true;
    //            getchar();
    //        }
    //        secondStubPosition++;
    //        if(secondStubPosition == NMAX_STUB_IN_CONTAINER) break;
    //        if(secondStubPosition == stubIndex - 1) break;
    //    }
    //}

    for(unsigned int i = 0; i < stubIndex; i++) {
        if(stubIsBad[i] == false) {
            cleanedStubs.push_back(stubs[i]);
        }
    }
}

void StubContainer::fillTree() {
    bx_stubs.clear();
    handler->clearVectors();
    //std::cout << "Filling with " << cleanedStubs.size() << std::endl;
    for (size_t i = 0; i < cleanedStubs.size(); i++)
    {
        uint32_t currentTimeStamp = cleanedStubs[i].getTimeStamp();
        uint32_t newTimeStamp = currentTimeStamp;
        while(newTimeStamp == currentTimeStamp) {
            bx_stubs.push_back(cleanedStubs[i]);
            i++;
            if(i==cleanedStubs.size()) break;
            newTimeStamp = cleanedStubs[i].getTimeStamp();
        }
        if(bx_stubs.size()>0) {
            for (auto stub : bx_stubs) {
                handler->pushBend(stub.getBend());
                handler->pushBx(stub.getBx());
                handler->pushLink(stub.getLink());
                handler->pushSuperID(stub.getSuperID());
                handler->pushPersistency(stub.getPersistency());
                handler->pushLocalX(stub.getLocalX());
                handler->pushLocalY(stub.getLocalY());
                if(stub.getUserBits() != 0xd451d007) {
                    handler->pushUserBits(stub.getUserBits());
                    handler->pushModuleID(stub.getModuleID());
                    handler->pushThreshold(stub.getThreshold());
                }
            }
            handler->fill();
            handler->clearVectors();
            bx_stubs.clear();
            i--;
        }
    }
    //std::cout << "\n";
    //handler->printNEntries();
}



void StubContainer::reset() {
    stubIndex = 0;    
    cleanedStubs.clear();
    resetBadStubs();
}

void StubContainer::resetBadStubs() {
    for(unsigned int i = 0; i<NMAX_STUB_IN_CONTAINER; i++) stubIsBad[i] = false;
}

std::vector<Stub> StubContainer::getCleanedStubs() {
    return cleanedStubs;
}

void StubContainer::clearCleanedStubs() {
    cleanedStubs.clear();
}

StubContainer::~StubContainer() {
    //delete handler;
}
