#pragma once
#include <array>
#include <vector>
#include <iostream>

const int NUMBER_OF_MODULES = 12;
const int NUMBER_OF_BX_PERSISTENCY = -1;
const int NUMBER_OF_BX_TO_CORRELATE = -1;
const int NUMBER_OF_BX_SYNC_WINDOW = 10;
const int NUMBER_OF_MAX_BX_IN_SUPERID = 3564;
const unsigned int BAD_BENDCODE = 8;
const unsigned int MINIMUM_NUMBER_STUBS_PER_PACKET = 1;
const unsigned int NUMBER_OF_BX_PER_PACKET = 8;
const unsigned int NUMBER_OF_STUBS_PER_BX_PER_MODULE = 1;

const int NSTRIPS = 1016;
const double STRIP_PITCH = 0.009;                       // cm (90um)
const double FULL_DIMENSIONS_X = NSTRIPS * STRIP_PITCH; // cm. full dimensions of a 2S module along the measured coordinate (9.144cm)
const double FULL_DIMENSIONS_Y = 10;                    // cm. full dimensions of a 2S module along strip axis (2 sets of strips, each set is 5cm long)



class Stub {

private:

    uint32_t superID    = 999;
    uint32_t userBits   = 999;
    uint16_t bx         = 999;
    float bend          = -999;
    float localX        = -999.; 
    float localY        = -999.;
    uint16_t link = 999;
    uint16_t moduleID = 999;
    uint16_t threshold = 999;
    uint16_t persistency = 0;

public:
    Stub() {};
    Stub(
        uint32_t superID_,
        uint16_t bx_,
        float bend_,
        float localX_,
        float localY_,
        uint16_t link_
    ) { 
        superID     = superID_;
        bx          = bx_;
        bend        = bend_;
        localX      = localX_;
        localY      = localY_;
        link = link_;
    }


    void print() const {
        std::cout << 
        "SuperID    " << superID     << "\n" <<
        "Bx         " << bx          << "\n" << 
        "Bend       " << bend        << "\n" <<
        "LocalX     " << localX      << "\n" <<
        "LocalY     " << localY      << "\n";
    }
    uint32_t getSuperID() const { return superID; }
    uint32_t getUserBits() const { return userBits; }
    uint16_t getBx() const {return bx;}
    uint16_t getLink() const {return link;}
    uint16_t getPersistency() const {return persistency;}
    float getBend() const {return bend;}
    float getLocalX() const {return localX;}
    float getLocalY() const {return localY;}      
    float getThreshold() const {return threshold;}      
    float getModuleID() const {return moduleID;}      
    
    void setSuperID(uint32_t sid) {superID = sid;}
    void setBx(uint16_t bx_) {
        bx = bx_;
    }
    void setLocalX(float localX_) {localX = localX_;}
    void setUserbits(uint32_t userBits_){
        if (userBits_ != 0xd451d007) {
            //std::cout << "Hey, it's me again " << std::endl;
            userBits = userBits_;
            moduleID = userBits_ >> 16;
            threshold = userBits_ & 0x0000ffff;
        }

    }
    int isNBxDistantFrom(Stub secondStub);
    void setPersistency(uint16_t persistency_) {persistency = persistency_;}
    bool isInTheSameLocationAs(Stub secondStub);
    bool isBefore(Stub secondStub);
    bool isAfter(Stub secondStub);
    void isPresentInASubsequentBx();
    void clearPersistency();
    void print();
    uint64_t getTimeStamp();
};
