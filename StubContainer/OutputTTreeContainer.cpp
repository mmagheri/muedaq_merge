#include "OutputTTreeContainer.hpp"

OutputTTreeContainer::OutputTTreeContainer(std::string filename)
{
    myFile = TFile::Open(filename.c_str(), "RECREATE");
    eventTree = new TTree("cbmsim", "cbmsim");
    TList *tl = new TList();
    tl->SetOwner();
    tl->AddLast(new TObjString("Persistency"));
    tl->AddLast(new TObjString("Bend"));
    tl->AddLast(new TObjString("Bx"));
    tl->AddLast(new TObjString("Link"));
    tl->AddLast(new TObjString("SuperID"));
    tl->AddLast(new TObjString("LocalX"));
    tl->AddLast(new TObjString("LocalY"));
    tl->Write("BranchList", 1);
    delete tl;
    eventTree->Branch("Persistency", &stub_Persistency);
    eventTree->Branch("Bend", &stub_Bend);
    eventTree->Branch("Bx", &stub_Bx);
    eventTree->Branch("Link", &stub_Link);
    eventTree->Branch("SuperID", &stub_SuperID);
    eventTree->Branch("LocalX", &stub_LocalX);
    eventTree->Branch("LocalY", &stub_LocalY);
}

OutputTTreeContainer::OutputTTreeContainer() {}

OutputTTreeContainer::~OutputTTreeContainer() {
    delete stub_Persistency;
    delete stub_Bx;
    delete stub_SuperID;
    delete stub_LocalX;
    delete stub_LocalY;
    delete stub_Bend;
    delete stub_Link;
    delete stub_UserBits;
    delete stub_ModuleID;
    delete stub_Threshold;

    delete eventTree;
    delete myFile;
}

void OutputTTreeContainer::clearVectors() {
    stub_Persistency->clear();
    stub_Bend->clear();
    stub_Bx->clear();
    stub_SuperID->clear();
    stub_LocalX->clear();
    stub_Link->clear();
    stub_LocalY->clear();
    stub_UserBits->clear();
    stub_ModuleID->clear();
    stub_Threshold->clear();
}

void OutputTTreeContainer::write() {
    eventTree->Print();
    myFile->cd();
    eventTree->Write();
}

void OutputTTreeContainer::fill() {
    eventTree->Fill();
}

void OutputTTreeContainer::printNEntries() {
    std::cout << "tree has: " << eventTree->GetEntries() << " events" << std::endl;
}

void OutputTTreeContainer::pushSuperID(uint32_t superid)    {stub_SuperID->push_back(superid);}
void OutputTTreeContainer::pushBx(uint16_t bx)              {stub_Bx->push_back(bx);}
void OutputTTreeContainer::pushLink(uint16_t link)          {stub_Link->push_back(link);}
void OutputTTreeContainer::pushBend(float bend)             {stub_Bend->push_back(bend);}
void OutputTTreeContainer::pushPersistency(uint16_t persistency)    {stub_Persistency->push_back(persistency);}
void OutputTTreeContainer::pushLocalX(float localx)         {stub_LocalX->push_back(localx);}
void OutputTTreeContainer::pushLocalY(float localy)         {stub_LocalY->push_back(localy);}
void OutputTTreeContainer::pushUserBits(uint32_t userBits_)         {stub_UserBits->push_back(userBits_);}
void OutputTTreeContainer::pushModuleID(uint32_t moduleID_)         {stub_ModuleID->push_back(moduleID_);}
void OutputTTreeContainer::pushThreshold(uint32_t threshold_)         {stub_Threshold->push_back(threshold_);}

void OutputTTreeContainer::setVcthScan(bool scan_) {
    isVcthScan = scan_;
    eventTree->Branch("UserBits", &stub_UserBits);
    eventTree->Branch("ModuleID_vcthScan", &stub_ModuleID);
    eventTree->Branch("Threshold_vcthScan", &stub_Threshold);
}

