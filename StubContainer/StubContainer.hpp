#pragma once
#include "Stub.hpp"
#include <cstdlib>
#include <vector>
#include <algorithm>
#include "OutputTTreeContainer.hpp"
#include <chrono>
#include <limits>

const unsigned int NMAX_STUB_IN_CONTAINER = 1000*16*25*NUMBER_OF_MODULES*64;
//const uint64_t NMAX_STUB_IN_CONTAINER = std::numeric_limits<uint32_t>::max() * 10;
//const uint64_t NMAX_STUB_IN_CONTAINER = 9223372036854775807; //max size available for the arch used in lxplus


class StubContainer {
private:
    std::array<Stub, NMAX_STUB_IN_CONTAINER> stubs;
    bool stubIsBad[NMAX_STUB_IN_CONTAINER];
    unsigned int stubIndex = 0;
    unsigned int nCycle = 0;
    std::vector<Stub> cleanedStubs;
    std::vector<Stub> bx_stubs;
    OutputTTreeContainer* handler;
    std::chrono::steady_clock::time_point st;

public:
    StubContainer(OutputTTreeContainer* handler_);
    StubContainer();
    ~StubContainer();
    void addStub(Stub theStub);
    void removePersistencyAndBadStubs();
    bool compareStubsTime(Stub stub1, Stub stub2);
    void printStubTiming();
    void printCleanStubTiming();
    void setFirstSuperIDBx(unsigned theSid, int theBx);
    int calculateDeltaTimeInContainer();
    void printFirstStub();
    void printLastStub();
    std::vector<Stub> extractFirstNBx(int nBx);
    std::vector<Stub> getCleanStubs() {return cleanedStubs;};
    void fillTree();
    void reset();
    void resetBadStubs();
    void cleanStubsAndFillTree();
    std::vector<Stub> getCleanedStubs();
    void clearCleanedStubs();
};
