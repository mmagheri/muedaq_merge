import os
import ROOT
import concurrent.futures
import argparse
import json
import re
import threading


folder_run1 = "/eos/experiment/mu-e/staging/daq/2023/decoded/commissioning/run_4018/"
folder_run2 = "/eos/experiment/mu-e/staging/daq/2023/decoded/commissioning/run_4017/"
folder_merged = "/eos/user/m/mmagheri/run1_merged_fixed/"

def get_map_with_range(start, end):
    return {i: 0 for i in range(start, end+1)}

def getEntriesInRange(fileName, superIDRange, map):
    file = ROOT.TFile(fileName)
    if not file or file.IsZombie():
        print(f"Error: Unable to open the file '{fileName}'")
        return []

    tree = file.Get("cbmsim")  # Replace 'your_tree_name' with the actual tree name in your file

    if not tree:
        print(f"Error: Unable to find the TTree in '{fileName}'. Skipping.")
        file.Close()
        return []

    superIDBranch = ROOT.std.vector("unsigned int")()
    tree.SetBranchAddress("SuperID", superIDBranch)

    nCorrectEntries = 0

    for i in range(tree.GetEntries()):
        tree.GetEntry(i)
        superID = superIDBranch[0]
        if superID >= superIDRange[0] and superID <= superIDRange[1]:
            map[superID] += len(superIDBranch)

    file.Close()
    #if(nCorrectEntries != 0):
    return nCorrectEntries

# BEGIN: 8f6d5a5d8j3d
def get_difference_map(map1, map2):
    return {k: map1[k] - map2[k] for k in map1}

def get_sum_map(map1, map2):
    return {k: map1[k] + map2[k] for k in map1}

def test(j):

    with open(j, 'r') as file:
        data = json.load(file)
    
    print(data)
    
    start = data["start"]
    end = data["end"]
    range_ = [start, end]

    map1 = get_map_with_range(start, end)
    map2 = get_map_with_range(start, end)
    mapmerged = get_map_with_range(start, end)

    file_list_run1 = [file.replace('.dat', '.root').replace('raw', 'decoded') for file in data['run_4018']]
    file_list_run2 = [file.replace('.dat', '.root').replace('raw', 'decoded') for file in data['run_4017']]

    file_list_merged = [f"{folder_merged}{data['start']}_{data['end']}.root"]
    for f in file_list_run1 :
        getEntriesInRange(f, range_, map1)
    for f in file_list_run2:
        getEntriesInRange(f, range_, map2)
    for f in file_list_merged:
        getEntriesInRange(f, range_, mapmerged)

    #print("map1: ", map1)   
    #print("map2: ", map2)
    #print("mapmerged: ", mapmerged)
    
    sumMap = get_sum_map(map1, map2)
    #print("differenceMap: ", sumMap)
    totalDifference = get_difference_map(sumMap, mapmerged)
    #print("mapmerged: ", totalDifference)
    #print only entries with difference
    print("Differences:")
    for k in totalDifference:
        if(totalDifference[k] != 0):
            print("SuperID: ", k, " difference: ", totalDifference[k])
# END: 8f6d5a5d8j3d
    
def main():
        
    parser = argparse.ArgumentParser(description="Count SuperID values in a folder of ROOT files.")
    parser.add_argument("folder", help="Path to the folder containing ROOT files")
    args = parser.parse_args()

    folder_path = args.folder

    for filename in os.listdir(folder_path):
        if filename.endswith(".json"):
            file_path = os.path.join(folder_path, filename)
            
            # Do something with the JSON file
            test(file_path)

if __name__ == "__main__":
    main()