import os
import ROOT

def CountVectorEntries(fileName, branchName):
    file = ROOT.TFile(fileName)
    if not file or file.IsZombie():
        print(f"Error: Unable to open the file '{fileName}'")
        return -1

    tree = file.Get("cbmsim")  # Replace 'your_tree_name' with the actual tree name in your file
    if not tree:
        print("Error: Unable to find the TTree in the file.")
        file.Close()
        return -1

    vectorBranch = ROOT.std.vector("unsigned short")()
    tree.SetBranchAddress(branchName, vectorBranch)

    totalEntries = tree.GetEntries()
    nVecEntries = 0
    for i in range(totalEntries):
        tree.GetEntry(i)
        nVecEntries += len(vectorBranch)

    print(f"'{fileName}'\t {nVecEntries}")

    file.Close()
    return nVecEntries


def CountVectorEntriesInFolder(folderPath, branchName):
    totalEntries = 0
    for root, dirs, files in os.walk(folderPath):
        for file in files:
            if file.endswith(".root"):
                filePath = os.path.join(root, file)
                entries =  CountVectorEntries(filePath, branchName)
                if entries != -1:
                    totalEntries += entries
    print(f"'{folderPath}' \t {totalEntries}")
    return totalEntries


def TestEntries():
    entriesMerged = CountVectorEntriesInFolder("/eos/user/m/mmagheri/run1_merged_fixed/", "Bx")  # Replace 'your_file.root' and 'Bx' as needed
    entries_r1 = CountVectorEntriesInFolder("/eos/experiment/mu-e/staging/daq/2023/decoded/commissioning/run_4018/", "Bx")  # Replace 'your_file.root' and 'Bx' as needed
    entries_r2 = CountVectorEntriesInFolder("/eos/experiment/mu-e/staging/daq/2023/decoded/commissioning/run_4017/", "Bx")  # Replace 'your_file.root' and 'Bx' as needed
    print("R1: ", entries_r1, " entries_r2: ", entries_r2, " entriesMerged: ", entriesMerged)
    print("Difference in entries: " , entriesMerged - entries_r1 - entries_r2)
TestEntries()

