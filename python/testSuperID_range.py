import os
import ROOT
import concurrent.futures
import argparse
import json
import re
import threading


folder_run1 = "/eos/experiment/mu-e/staging/daq/2023/decoded/commissioning/run_4018/"
folder_run2 = "/eos/experiment/mu-e/staging/daq/2023/decoded/commissioning/run_4017/"
folder_merged = "/eos/user/m/mmagheri/merged_run1/"

# BEGIN: xz78d4fghjkl
def getEntriesInRange(fileName, superIDRange):
    file = ROOT.TFile(fileName)
    if not file or file.IsZombie():
        print(f"Error: Unable to open the file '{fileName}'")
        return []

    tree = file.Get("cbmsim")  # Replace 'your_tree_name' with the actual tree name in your file

    if not tree:
        print(f"Error: Unable to find the TTree in '{fileName}'. Skipping.")
        file.Close()
        return []

    superIDBranch = ROOT.std.vector("unsigned int")()
    tree.SetBranchAddress("SuperID", superIDBranch)

    nCorrectEntries = 0

    for i in range(tree.GetEntries()):
        tree.GetEntry(i)
        superID = superIDBranch[0]
        if superID >= superIDRange[0] and superID <= superIDRange[1]:
            nCorrectEntries += len(superIDBranch)

    file.Close()
    #if(nCorrectEntries != 0):
    print(f"Correct entries in {fileName}: {nCorrectEntries}")
    return nCorrectEntries
# END: xz78d4fghjkl

def getEntriesInRangeInFolder(folder_path, superIDRange):
    files_list = []
    # Define a variable to store the total number of correct entries
    total_correct_entries = 0
    # Loop through all files in the folder
    for filename in os.listdir(folder_path):
        if filename.endswith(".root"):
            # Get the full file path
            file_path = os.path.join(folder_path, filename)
            # Call the getEntriesInRange function and add the result to the total
            entriesInFile = getEntriesInRange(file_path, superIDRange)
            if(entriesInFile != 0) : 
                total_correct_entries += entriesInFile
                files_list.append(file_path)
    #print(f"Total number of correct entries: {total_correct_entries}")
    return [total_correct_entries, files_list]

def process_data(file_path):
    with open(file_path, 'r') as f:
        data = json.load(f)
    start = data["start"]
    end = data["end"]
    runs = {}

    for key, value in data.items():
        if key.startswith("run_"):
            runs[key] = [file.split('/')[-1].split('.')[0] for file in value]

    return [start, end] , runs

def processFromJson(jsonFolder):
    for filename in os.listdir(jsonFolder):
        print("Analyzing:  ", filename)
        if filename.endswith(".json"):
            # Do something with the file
            superIDRange, fileNames = process_data(os.path.join(jsonFolder, filename))

            def get_entries(folder, superIDRange, results, index):
                entries, files = getEntriesInRangeInFolder(folder, superIDRange)
                results[index] = (entries, files)

            results = [None, None, None]

            # Start threads
            t1 = threading.Thread(target=get_entries, args=(folder_run1, superIDRange, results, 0))
            t2 = threading.Thread(target=get_entries, args=(folder_run2, superIDRange, results, 1))
            t3 = threading.Thread(target=get_entries, args=(folder_merged, superIDRange, results, 2))

            t1.start()
            t2.start()
            t3.start()

            # Wait for threads to finish
            t1.join()
            t2.join()
            t3.join()

            entries_run1, files_run1 = results[0]
            entries_run2, files_run2 = results[1]
            entries_merged, files_merged = results[2]

            differenceInEntries = entries_merged - entries_run1 - entries_run2
            if(differenceInEntries!= 0):
                print("Difference in entries: " , differenceInEntries)
                for file_run1 in files_run1:
                    if not(any(file_run1.split('/')[-1].split('.')[0] in file_name for file_name in fileNames["run_4018"])):
                        print(f"{file_run1} is NOT contained in fileNames['run_4018']")
                    for file_run2 in files_run2:
                        if not(any(file_run2.split('/')[-1].split('.')[0] in file_name for file_name in fileNames["run_4017"])):
                            print(f"{file_run1} is NOT contained in fileNames['run_4017']")

def main():
    processFromJson("/afs/cern.ch/user/m/mmagheri/workspace/muedaq_merge/jsonSetupFiles/run1")

if __name__ == "__main__":
    main()

def main():
    # Define the folder path
    #a,c =process_data("/afs/cern.ch/user/m/mmagheri/workspace/muedaq_merge/jsonSetupFiles/run1/7614793_7664197.json")
    #print(a)
    #print(c)
    #folder_run1 = "/eos/experiment/mu-e/staging/daq/2023/decoded/commissioning/run_4018/"
    #folder_run2 = "/eos/experiment/mu-e/staging/daq/2023/decoded/commissioning/run_4017/"
    #folder_merged = "/eos/user/m/mmagheri/merged_run1/"
    processFromJson("/afs/cern.ch/user/m/mmagheri/workspace/muedaq_merge/jsonSetupFiles/run1")

if __name__ == "__main__":
    main()


