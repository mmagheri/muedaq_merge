import os
import ROOT
import concurrent.futures
import argparse

def count_super_id_in_file(fileName, minSuperID, maxSuperID, result_lock, result_list):
    count = CountSuperIDInRange(fileName, minSuperID, maxSuperID)
    if count != -1:
        with result_lock:
            result_list.append(count)

def CountSuperIDInRange(fileName, minSuperID, maxSuperID):
    file = ROOT.TFile(fileName)
    if not file or file.IsZombie():
        print(f"Error: Unable to open the file '{fileName}'")
        return -1

    tree = file.Get("cbmsim")  # Replace 'your_tree_name' with the actual tree name in your file

    if not tree:
        print(f"Error: Unable to find the TTree in '{fileName}'. Skipping.")
        file.Close()
        return -1

    superIDBranch = ROOT.std.vector("unsigned int")()
    tree.SetBranchAddress("SuperID", superIDBranch)

    count = 0
    totalEntries = tree.GetEntries()

    if totalEntries == 0: 
        print("Zero entries ", fileName, " : ", 0)
        file.Close()
        return 0
    
    tree.GetEntry(0)
    if superIDBranch[0] > maxSuperID:
        print("superID: ", superIDBranch[0], " maxSuperID: ", maxSuperID)
        print("First entry higher than maxSuperID : ", fileName, " : ", 0)
        file.Close()
        return 0

    tree.GetEntry(totalEntries - 1)
    if superIDBranch[0] < minSuperID:
        print("superID: ", superIDBranch[0], " minSuperID: ", minSuperID)
        print("last entry less than minSuperID ", fileName, " : ", 0)

        file.Close()
        return 0

    for entry in range(totalEntries):
        tree.GetEntry(entry)
        for superID in superIDBranch:
            if minSuperID <= superID <= maxSuperID:
                count += 1
    print(fileName," : ", count)
    file.Close()

    return count


def CountSuperIDInRangeInFolder(folderPath, minSuperID, maxSuperID):
    total_count = 0
    file_list = []

    for root, dirs, files in os.walk(folderPath):
        for file in files:
            if file.endswith(".root"):
                filePath = os.path.join(root, file)
                file_list.append(filePath)

    # Determine the number of threads based on available CPU cores
    max_threads = os.cpu_count()

    with concurrent.futures.ThreadPoolExecutor(max_threads) as executor:
        futures = [executor.submit(CountSuperIDInRange, file, minSuperID, maxSuperID) for file in file_list]

        for future in concurrent.futures.as_completed(futures):
            count = future.result()
            if count != -1:
                total_count += count

    print(f"Total entries with SuperID in range [{minSuperID}, {maxSuperID}] for all files in '{folderPath}': {total_count}")

def main():
    parser = argparse.ArgumentParser(description="Count SuperID values in a folder of ROOT files.")
    parser.add_argument("folder", help="Path to the folder containing ROOT files")
    parser.add_argument("minSuperID", type=int, help="Minimum SuperID value")
    parser.add_argument("maxSuperID", type=int, help="Maximum SuperID value")
    args = parser.parse_args()

    folderPath = args.folder
    minSuperID = args.minSuperID
    maxSuperID = args.maxSuperID
    print(maxSuperID, " min: ", minSuperID)
    CountSuperIDInRangeInFolder(folderPath, minSuperID, maxSuperID)

if __name__ == "__main__":
    main()

