import htcondor
import os
import sys

output_directory = "/eos/experiment/mu-e/staging/daq/2023/merged/test_fix_efficiency/"

def chunk_list(input_list, chunk_size):
    for i in range(0, len(input_list), chunk_size):
        yield input_list[i:i + chunk_size]

def filter_existing_files(files, run_number):
    """
    Filters out filenames from the given list that already exist in the specified path.
    
    Args:
    - files (list): List of dictionaries containing filenames to be checked.
    - run_number (str): The run number used to build the path.
    
    Returns:
    - List of dictionaries containing filenames that don't exist in the specified path.
    """
    filtered_files = []
    path_template = output_directory + "/run_{}/{}.root"
    
    for file_dict in files:
        filename = file_dict["filename"]
        if not os.path.exists(path_template.format(run_number, filename)):
            filtered_files.append(file_dict)
    
    return filtered_files


# Check for command line argument
if len(sys.argv) != 2:
    print("Usage: python script_name.py <run_number>")
    sys.exit(1)
    
run_number = sys.argv[1]

# Path to the directory containing the .json files
directory_path = f"/afs/cern.ch/user/m/mmagheri/workspace/muedaq_merge/jsonSetupFiles/test_fix_efficiency/run_{run_number}/"

# Get all filenames without the .json extension
files_ = [f[:-5] for f in os.listdir(directory_path) if f.endswith('.json')]

tmpfiles = []
for f in files_:
    tmpfiles.append(
        {
            "filename": f
        }
    )
files = filter_existing_files(tmpfiles, run_number)

col = htcondor.Collector()
credd = htcondor.Credd()
credd.add_user_cred(htcondor.CredTypes.Kerberos, None)
schedd = htcondor.Schedd()

# Create a new submit object
sub = htcondor.Submit()

# Set the submit parameters
sub["Universe"] = "vanilla"
sub["Executable"] = "build/apps/mergeFromJson"
sub["Arguments"] = f"-j $(filename).json -o $(filename).root --decodedPath /eos/experiment/mu-e/staging/daq/2023/decoded/commissioning/test_fix_efficiency/"
sub["transfer_input_files"] = f"/afs/cern.ch/user/m/mmagheri/workspace/muedaq_merge/jsonSetupFiles/test_fix_efficiency/run_{run_number}/$(filename).json"
sub["transfer_output_files"] = "$(filename).root"
#sub["MY.WantOS"] = "el7"
sub["Requirements"] = "(OpSysAndVer =?= \"CentOS7\")"
sub["should_transfer_files"] = "YES"
sub["when_to_transfer_output"] = "ON_EXIT"
sub["+JobFlavour"] = '"tomorrow"'
sub["getenv"] = "True"
sub["Log"] = f"condor_utils/condor_run/run_{run_number}/log/$(filename).log"
sub["Output"] = f"condor_utils/condor_run/run_{run_number}/output/$(filename).out"
sub["Error"] = f"condor_utils/condor_run/run_{run_number}/error/$(filename).err"
sub["request_memory"] = "4000"
sub["transfer_output_remaps"] = '"$(filename).root = root://eosexperiment.cern.ch/' + output_directory + f'run_{run_number}/$(filename).root"'
sub["MY.XRDCP_CREATE_DIR"] = "True"
sub["MY.SendCredential"] = "True"

file_limit = 5000
chunked_files = list(chunk_list(files, file_limit))
for chunk in chunked_files:
    submit_result = schedd.submit(sub, itemdata = iter(chunk))
    print("submitted: ", len(chunk), " jobs")
