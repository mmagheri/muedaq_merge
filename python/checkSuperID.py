import ROOT

def CheckSuperIDGrowth(fileName):
    file = ROOT.TFile(fileName)
    if not file or file.IsZombie():
        print(f"Error: Unable to open the file '{fileName}'")
        return False

    tree = file.Get("cbmsim")  # Replace 'your_tree_name' with the actual tree name in your file
    if not tree:
        print(f"Error: Unable to find the TTree in '{fileName}'.")
        file.Close()
        return False

    superIDBranch = ROOT.std.vector("unsigned int")()
    tree.SetBranchAddress("SuperID", superIDBranch)

    prevSuperID = None
    totalEntries = tree.GetEntries()

    for entry in range(totalEntries):
        tree.GetEntry(entry)
        for superID in superIDBranch:
            if prevSuperID is not None and superID < prevSuperID:
                print(f"SuperID is not always growing at entry {entry}.")
                file.Close()
                return False
            prevSuperID = superID

    print("SuperID is always growing in the file.")
    file.Close()
    return True

def YourProgram(fileName):
    result = CheckSuperIDGrowth(fileName)
    if result:
        print(f"'{fileName}' has SuperID values always growing.")
    else:
        print(f"'{fileName}' does not have SuperID values always growing.")

# Replace 'your_file.root' with the actual file name you want to check
YourProgram("/eos/experiment/mu-e/staging/daq/2023/decoded/commissioning/run_4017/4017_014d620e1_014d660e0_4200000.root")

