import requests
import json
import sys

if len(sys.argv) != 4:
    print("Please provide the run number as a command line argument.")
    exit()

n_r1 = sys.argv[1]
n_r2 = sys.argv[2]

outFileName = sys.argv[3]

print("Run number:", n_r1, " ", n_r2)
print((f"http://muedaq-supervisor/api/v1/data/raw/{n_r1}"))
print((f"http://muedaq-supervisor/api/v1/data/raw/{n_r2}"))
run1 = requests.get(f"http://muedaq-supervisor/api/v1/data/raw/{n_r1}").json()
run2 = requests.get(f"http://muedaq-supervisor/api/v1/data/raw/{n_r2}").json()

print(len(run1["data"]))
init_files = len(run1["data"])
run1["data"] = [entry for entry in run1["data"] if "superIDs" in entry and entry["superIDs"]["first"] is not None and entry["superIDs"]["last"] is not None]
end_files = len(run1["data"])
print("Skipped: " + str(init_files - end_files) + " files for run1")
print("finished run1")

init_files = len(run2["data"])
run2["data"] = [entry for entry in run2["data"] if "superIDs" in entry and entry["superIDs"]["first"] is not None and entry["superIDs"]["last"] is not None]
superIDs = [entry["superIDs"]["first"] for entry in run1["data"]] + [entry["superIDs"]["last"] for entry in run1["data"]] + [entry["superIDs"]["last"] for entry in run2["data"]] + [entry["superIDs"]["last"] for entry in run2["data"]]
end_files = len(run2["data"])
print("Skipped: " + str(init_files - end_files) + " files for run2")

absolute_lowest_superID = min(superIDs)
absolute_highest_superID = max(superIDs)

print("Absolute lowest superID:", absolute_lowest_superID)
print("Absolute highest superID:", absolute_highest_superID)

numberRun1 = "run_" + str(n_r1)
numberRun2 = "run_" + str(n_r2)
# Split the superID range into len(r["data"]) non-overlapping ranges
num_ranges = (len(run1["data"]) + len(run2["data"])) * 20

range_size = (absolute_highest_superID - absolute_lowest_superID) // num_ranges
ranges = [(i * range_size + absolute_lowest_superID + 1 , (i + 1) * range_size + absolute_lowest_superID) for i in range(1, num_ranges)]
ranges[-1] = (ranges[-1][0], absolute_highest_superID + 1)  # adjust the last range to include the highest superID
ranges[0] = (absolute_lowest_superID, ranges[0][1])  # adjust the last range to include the highest superID
print("Number of ranges:", len(ranges))
for i in range(10):
    print(ranges[i])
outfiles = []

for sidrange in ranges:
    tmpOutputFiles = {numberRun1: [], numberRun2: []}

    for file in run1["data"]:
        if file["superIDs"]["first"] >= sidrange[0] and file["superIDs"]["first"] <= sidrange[1]:
            tmpOutputFiles[numberRun1].append(file["path"])
        elif file["superIDs"]["last"] >= sidrange[0] and file["superIDs"]["last"] <= sidrange[1]:
            tmpOutputFiles[numberRun1].append(file["path"])
    for file in run2["data"]:
        if file["superIDs"]["first"] >= sidrange[0] and file["superIDs"]["first"] <= sidrange[1]:
            tmpOutputFiles[numberRun2].append(file["path"])
        elif file["superIDs"]["last"] >= sidrange[0] and file["superIDs"]["last"] <= sidrange[1]:
            tmpOutputFiles[numberRun2].append(file["path"])
    if(len(tmpOutputFiles[numberRun1]) != 0 or len(tmpOutputFiles[numberRun2]) != 0):
        tmpOutputFiles["start"] = sidrange[0]
        tmpOutputFiles["end"] = sidrange[1]
        tmpOutputFiles["run_1"] = n_r1
        tmpOutputFiles["run_2"] = n_r2
        outfiles.append(tmpOutputFiles)

outputFilesJson = json.dumps(outfiles, indent = 4)
with open(outFileName, "w") as outfile:
    outfile.write(outputFilesJson)