import json
import sys

run_number = sys.argv[1]
with open(f'/afs/cern.ch/user/m/mmagheri/workspace/muedaq_merge/run_{run_number}.json', 'r') as f:
    input_data = json.load(f)

for i, entry in enumerate(input_data):
    output_data = {k: entry[k] for k in entry}
    with open(f"/afs/cern.ch/user/m/mmagheri/workspace/muedaq_merge/jsonSetupFiles/test_fix_efficiency/run_{run_number}/{entry['start']}_{entry['end']}.json", 'w') as f:
        json.dump(output_data, f, indent=4)
