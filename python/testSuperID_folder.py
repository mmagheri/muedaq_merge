import os
import ROOT
import concurrent.futures
import argparse
import re

def extract_superIDs(filename):
    match = re.search(r'(\d+)_(\d+).root', filename)
    if match:
        first_superID = int(match.group(1))
        last_superID = int(match.group(2))
        print("SuperIDs fetched from filename:")
        print ("first_superID: ", first_superID, " last_superID: ", last_superID)
        return [first_superID, last_superID]

    else:
        return [None, None]

def getSuperIDRange(fileName):
    file = ROOT.TFile(fileName)
    if not file or file.IsZombie():
        print(f"Error: Unable to open the file '{fileName}'")
        return -1

    tree = file.Get("cbmsim")  # Replace 'your_tree_name' with the actual tree name in your file

    if not tree:
        print(f"Error: Unable to find the TTree in '{fileName}'. Skipping.")
        file.Close()
        return -1

    superIDBranch = ROOT.std.vector("unsigned int")()
    tree.SetBranchAddress("SuperID", superIDBranch)

    count = 0
    totalEntries = tree.GetEntries()

    if totalEntries == 0: 
        print("Zero entries ", fileName, " : ", 0)
        file.Close()
        return 0
    
    tree.GetEntry(0)
    minSuperID = superIDBranch[0]
    tree.GetEntry(totalEntries - 1)
    maxSuperID = superIDBranch[0]
    file.Close()

    return [minSuperID, maxSuperID]

def main():
    parser = argparse.ArgumentParser(description="Count SuperID values in a folder of ROOT files.")
    parser.add_argument("folder", help="Path to the folder containing ROOT files")
    args = parser.parse_args()
    
    folder_path = args.folder
    for file in os.listdir(folder_path):
        if file.endswith(".root"):
            file_path = os.path.join(folder_path, file)
            sIdRange = getSuperIDRange(file_path)
            print("sIdRange got from the tree for file", file, ":", sIdRange)
            superIDRangeFromFileName = extract_superIDs(file)
            # BEGIN: 8j3d9f4j3d9f
            if sIdRange == 0:
                print("sIdRange is 0 for file", file)
                continue
            if sIdRange[0] >= superIDRangeFromFileName[0] and sIdRange[1] <= superIDRangeFromFileName[1]:
                print("sIdRange is contained in superIDRangeFromFileName for file", file)
            else:
                print("\tERROR - sIdRange is not contained in superIDRangeFromFileName for file", file)
                return False
            # END: 8j3d9f4j3d9f
    
if __name__ == "__main__":
    main()

